
#include "include.h"

double** chargerFiltre(int type, int *taille)
/// charge un filtre du fichier et retourne un tableau 2D
{
    int i, j, k;
    double tmp, somme = 0;

    FILE *F = fopen("filtres/filtre", "r");
    if(F == NULL)
        fprintf(stderr, "\nErreur lors de la chargement du fichier des filtres");


    for(k = 0;k < type;k++)
    {
        fscanf(F, "%d", taille);
        for(i = 0;i < *taille;i++)
            for(j = 0;j < *taille;j++)
                fscanf(F, "%lf", &tmp);
    }

    fscanf(F, "%d", taille);
    double **filtre = malloc(sizeof(double*) * *taille);
    for(i = 0;i < *taille;i++)
        filtre[i] = malloc(sizeof(double) * *taille);
    for(i = 0;i < *taille;i++)
        for(j = 0;j < *taille;j++)
        {
            fscanf(F, "%lf", &filtre[i][j]);
            somme += filtre[i][j];
        }

    fclose(F);
    return filtre;
}

void appliquerFiltre(s_map *m, SDL_Rect r, int typeFiltre)
/// applique un le filtre typeFiltre à la zone r de la map m
{

    int i, j, k, l, a, b;
    int taille = 10;
    double somme;

    int **carte = malloc(sizeof(int*) * m->x);
    for(i = 0;i < m->x;i++)
        carte[i] = malloc(sizeof(int) * m->y);

    double **filtre = chargerFiltre(typeFiltre, &taille);
    for(i = 0;i < taille;i++)
    {
        fprintf(stderr, "\n");
        for(j = 0;j < taille;j++)
            fprintf(stderr, " %lf", filtre[i][j]);
    }
    taille = taille / 2;

    for(i = r.x;i < r.x + r.w;i++)
    {
        for(j = r.y;j < r.y + r.h;j++)
        {
            somme = 0;
            for(k = -taille;k < taille + 1;k++)
            {
                for(l = -taille;l < taille + 1;l++)
                {
                    /// méthode du zero-padding
                    /*if(caseVide(i + k, j + l, m, r) != EN_DEHORS)
                    {
                        somme = somme + ((m->carte[i + k][j + l].pos.c.z + 1) * filtre[k + taille][l + taille]);
                    }*/

                    /// méthode de la duplication
                    for(l = -taille;l < taille + 1;l++)
                    {
                        a = min(max(i + k, r.x), r.x + r.w - 1);
                        b = min(max(j + l, r.y), r.y + r.h - 1);
                        somme = somme + ((m->carte[a][b].pos.c.z + 1) * filtre[k + taille][l + taille]);
                    }
                }
            }
            carte[i][j] = max(min(arround(somme) - 1, 14), -1);
        }
    }

    for(i = r.x;i < r.x + r.w;i++)
        for(j = r.y;j < r.y + r.h;j++)
            changerTailleCase(i, j, carte[i][j], m);
}

















































