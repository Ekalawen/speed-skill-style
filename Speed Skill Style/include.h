
#ifndef INCLUDE_H
#define INCLUDE_H

    #include <stdio.h>
    #include <stdlib.h>
    #include <SDL.h>
    #include <SDL_image.h>
    #include <SDL_ttf.h>
    #include <time.h>
    #include <string.h>
    #include <math.h>
    //#include <SDL/SDL_rotozoom.h>
    //#include <SDL_rotozoom.h>

    #include "struct.h"
    #include "outil.h"
    #include "vision.h"
    #include "map.h"
    #include "joueur.h"
    #include "event.h"
    #include "deplacement.h"
    #include "collision.h"
    #include "transparence.h"
    #include "generator.h"
    #include "LCPF.h"
    #include "labyrinthe.h"
    #include "filtre.h"

#endif // INCLUDE_H



















