
// Gere les fonctions principales concernant la carte en elle-même :
// chargement, affichage, sauvegarde, libération

#include "include.h"


s_map* chargerMap(char file[])
{
    int i, j;
    char buf[STR_MAX];
    FILE *F = fopen(file, "r");
    if(F == NULL)
        fprintf(stderr, "\nErreur de chargement du fichier du niveau");
    s_map *m = malloc(sizeof(s_map));

    m->titre = malloc(sizeof(char) * STR_MAX);
    fscanf(F, "%s %s", buf, m->titre);
    fscanf(F, "%s %d %d", buf, &m->x, &m->y);

    fscanf(F, "%s %s", buf, buf);
    m->tilesetUni = IMG_Load(buf);
    if(m->tilesetUni == NULL)
        fprintf(stderr, "\nEchec de chargement du tileset plein");

    fscanf(F, "%s %s", buf, buf);
    m->tilesetTransparent = IMG_Load(buf);
    if(m->tilesetTransparent == NULL)
        fprintf(stderr, "\nEchec de chargement du tileset transparent");

    fscanf(F, "%s %s", buf, buf);
    m->tilesetPlat = IMG_Load(buf);
    if(m->tilesetPlat == NULL)
        fprintf(stderr, "\nEchec de chargement du tileset blanc");

    fscanf(F, "%s %s", buf, buf);
    m->fond = IMG_Load(buf);
    if(m->fond == NULL)
        fprintf(stderr, "\nEchec de chargement de l'image de fond");

    fscanf(F, "%s %d %d", buf, &m->depart.x, &m->depart.y);
    fscanf(F, "%s %d %d", buf, &m->arrivee.x, &m->arrivee.y);

    m->carte = malloc(sizeof(s_infoTile*) * m->x);
    for(i = 0;i < m->x;i++)
        m->carte[i] = malloc(sizeof(s_infoTile) * m->y);
    fscanf(F, "%s", buf);
    int k = 0, couleur = floor(rand() % 5 + 10);
    for(j = 0;j < m->y / 2;j++)
        for(i = 0;i < m->x / 2;i++)
    {
        couleur = floor(rand() % 5 + min(m->x / 2 - i, m->y / 2 - j));
        m->carte[2 * i][2 * j].pos.c.z = couleur;
        m->carte[2*i+1][2*j].pos.c.z = couleur;
        m->carte[2*i][2*j+1].pos.c.z = couleur;
        m->carte[2*i+1][2*j+1].pos.c.z = couleur;

        //m->carte[2 * i + 2][2 * j].pos.c.z = couleur;
        //m->carte[2 * i + 2 ][2 * j + 1].pos.c.z = couleur;
        //m->carte[2 * i][2 * j + 2].pos.c.z = couleur;
        //m->carte[2 * i+1][2 * j +2].pos.c.z = couleur;
        //m->carte[2 * i+2][2 * j+2].pos.c.z = couleur;

    }
    for(j = 0;j < m->y;j++)
        for(i = 0;i < m->x;i++)
            {
                m->carte[i][j].pos.c.x = i;
                m->carte[i][j].pos.c.y = j;
                k++;
                if(k >= 4)
                {
                    k = 0;
                    couleur = floor(rand() % 5 + 10);
                }
				// TODO
				// comment the next 2 lines to have a generation ! :D
                m->carte[i][j].pos.c.z = couleur;
                fscanf(F, "%d", &m->carte[i][j].pos.c.z);
                m->carte[i][j].pos.v.x = i * 80;
                m->carte[i][j].pos.v.y = j * 80;
                m->carte[i][j].pos.v.w = 80;
                m->carte[i][j].pos.v.h = 80;
                m->carte[i][j].pos.v.z = m->carte[i][j].pos.c.z;
                m->carte[i][j].pos.a = posVconvertPosA(m->carte[i][j].pos.v);

                m->carte[i][j].pos.R.x = 80 * m->carte[i][j].pos.c.z;
                m->carte[i][j].pos.R.y = 0;
                m->carte[i][j].pos.R.w = 80;
                m->carte[i][j].pos.R.h = 80 + m->carte[i][j].pos.c.z * 40;

                m->carte[i][j].etat = UNI;
                m->carte[i][j].affiche = 0;
            }

    fclose(F);
    return m;
}

void afficherMap(s_map *m, s_vision *v, s_joueur *joueur, SDL_Surface *ecran)
{
    int i = 0, j = 0, k = 0, continuer = 1;
    s_posA posA;
    //récupérer les cases sur lesquelles est le joueur
    s_posC *posC = casesOccuppeesParJoueur(m, joueur);
    int nbCases = 0;
    for(i = 0; i < 4;i++)
        if(posC[i].x != -1)
            nbCases ++;

    //afficher le fond
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
    posA.x = LARGEUR_ECRAN / 2 - m->fond->w / 2; posA.y = HAUTEUR_ECRAN / 2 - m->fond->h / 2;
    blit(m->fond, NULL, ecran, &posA);

    //afficher chaque case dans l'ordre diagonal en fonction de leur position et de leur type (transparente/plein/blanc)
    devoilerCase(m, joueur);
    transparenceMap(m, joueur);
    ajusterVision(v, joueur);
    i = 0;j = 0;
    while(continuer)
    {
        posA.x = m->carte[i][j].pos.a.x + v->posA.x;
        posA.y = m->carte[i][j].pos.a.y + v->posA.y;

        if(caseVisible(&posA, m->carte[i][j].pos.c.z) == 1)
        {
            if(m->carte[i][j].etat == UNI)
                blit(m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA);
            else if(m->carte[i][j].etat == TRANSPARENT)
                blitTransparent(m->tilesetPlat, m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA, m->carte[i][j].pos.c.z - joueur->pos.c.z);
                //blit(m->tilesetTransparent, &m->carte[i][j].pos.R, ecran, &posA);
            else if(m->carte[i][j].etat == PLAT)
                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
            //y intercaler le joueur, on regarde si toutes les cases qu'il occupe ont été affiché, quand la dernière l'est, on l'affiche
            for(k = 0;k < 4;k++)
            {
                if(m->carte[i][j].pos.c.x == posC[k].x
                   &&m->carte[i][j].pos.c.y == posC[k].y
                   &&m->carte[i][j].pos.c.z == posC[k].z)
                {
                    nbCases --;
                    if(nbCases == 0)
                    {
                        //cette condition sert juste à vérifier que la case juste après en diagonale ne s'affiche pas par dessus le joueur
                        if(!(i == m->x - 1 && j == m->y - 1))
                        {
                            parcourirEnDiagonale(&i, &j, m, &continuer);
                            posA.x = m->carte[i][j].pos.a.x + v->posA.x;
                            posA.y = m->carte[i][j].pos.a.y + v->posA.y;
                            if(m->carte[i][j].etat == UNI)
                                blit(m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA);
                            else if(m->carte[i][j].etat == TRANSPARENT)
                                blitTransparent(m->tilesetPlat, m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA, m->carte[i][j].pos.c.z - joueur->pos.c.z);
                                //blit(m->tilesetTransparent, &m->carte[i][j].pos.R, ecran, &posA);
                            else if(m->carte[i][j].etat == PLAT)
                                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                        }

                        posA.x = joueur->pos.a.x + v->posA.x;
                        posA.y = joueur->pos.a.y + v->posA.y;
                        blit(joueur->surface, NULL, ecran, &posA);
                        break;
                    }

                }
            }
        }
        parcourirEnDiagonale(&i, &j, m, &continuer);
    }
    //flip !
    SDL_Flip(ecran);
}
void afficherMapAerienne(s_map *m, s_vision *v, s_joueur *joueur, SDL_Surface *ecran)
{
    int i = 0, j = 0, k = 0, continuer = 1;
    s_posA posA;
    //récupérer les cases sur lesquelles est le joueur
    s_posC *posC = casesOccuppeesParJoueur(m, joueur);
    int nbCases = 0;
    for(i = 0; i < 4;i++)
        if(posC[i].x != -1)
            nbCases ++;

    //afficher le fond
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
    posA.x = LARGEUR_ECRAN / 2 - m->fond->w / 2; posA.y = HAUTEUR_ECRAN / 2 - m->fond->h / 2;
    blit(m->fond, NULL, ecran, &posA);

    //afficher chaque case dans l'ordre diagonal en fonction de leur position et de leur type (transparente/plein/blanc)
    devoilerCase(m, joueur);
    transparenceMap(m, joueur);
    ajusterVision(v, joueur);
    i = 0;j = 0;
    while(continuer)
    {
        posA.x = m->carte[i][j].pos.a.x + v->posA.x;
        posA.y = m->carte[i][j].pos.a.y + v->posA.y;

        if(caseVisible(&posA, m->carte[i][j].pos.c.z) == 1)
        {
            if(m->carte[i][j].etat == UNI)
            {
                //blit(m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA);
                posA.y -= 40 * m->carte[i][j].pos.c.z;
                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
            }
            else if(m->carte[i][j].etat == TRANSPARENT)
            {
                //blitTransparent(m->tilesetPlat, m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA, m->carte[i][j].pos.c.z - joueur->pos.c.z);
                //blit(m->tilesetTransparent, &m->carte[i][j].pos.R, ecran, &posA);
                posA.y -= 40 * m->carte[i][j].pos.c.z;
                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
            }
            else if(m->carte[i][j].etat == PLAT)
            {
                //blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                posA.y -= 40 * m->carte[i][j].pos.c.z;
                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
            }
            //y intercaler le joueur, on regarde si toutes les cases qu'il occupe ont été affiché, quand la dernière l'est, on l'affiche
            for(k = 0;k < 4;k++)
            {
                if(m->carte[i][j].pos.c.x == posC[k].x
                   &&m->carte[i][j].pos.c.y == posC[k].y
                   &&m->carte[i][j].pos.c.z == posC[k].z)
                {
                    nbCases --;
                    if(nbCases == 0)
                    {
                        //cette condition sert juste à vérifier que la case juste après en diagonale ne s'affiche pas par dessus le joueur
                        if(!(i == m->x - 1 && j == m->y - 1))
                        {
                            parcourirEnDiagonale(&i, &j, m, &continuer);
                            posA.x = m->carte[i][j].pos.a.x + v->posA.x;
                            posA.y = m->carte[i][j].pos.a.y + v->posA.y;
                            if(m->carte[i][j].etat == UNI)
                            {
                                //blit(m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA);
                                posA.y -= 40 * m->carte[i][j].pos.c.z;
                                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                            }
                            else if(m->carte[i][j].etat == TRANSPARENT)
                            {
                                //blitTransparent(m->tilesetPlat, m->tilesetUni, &m->carte[i][j].pos.R, ecran, &posA, m->carte[i][j].pos.c.z - joueur->pos.c.z);
                                //blit(m->tilesetTransparent, &m->carte[i][j].pos.R, ecran, &posA);
                                posA.y -= 40 * m->carte[i][j].pos.c.z;
                                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                            }
                            else if(m->carte[i][j].etat == PLAT)
                            {
                                //blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                                posA.y -= 40 * m->carte[i][j].pos.c.z;
                                blit(m->tilesetPlat, &m->carte[i][j].pos.R, ecran, &posA);
                            }
                        }

                        posA.x = joueur->pos.a.x + v->posA.x;
                        posA.y = joueur->pos.a.y + v->posA.y;
                        blit(joueur->surface, NULL, ecran, &posA);
                        break;
                    }

                }
            }
        }
        parcourirEnDiagonale(&i, &j, m, &continuer);
    }
    //flip !
    SDL_Flip(ecran);
}

void afficherCarteTerminal(s_map *m)
{
    int i, j;
    for(i = 0; i < m->x;i++)
    {
        fprintf(stderr, "\n");
        for(j = 0;j < m->y;j++)
            fprintf(stderr, " %d", m->carte[i][j].pos.c.z);
    }
}

void parcourirEnDiagonale(int *i, int *j, s_map *m, int *continuer)
///permet de parcourir en tableau de taille rectangulaire dans le sens de la diagonale
{
    ///refaire la fonction en prenant en compte les 3 cas possibles :
    /// m->x > m->y ; m->y > m->x ; m->x == m->y
    if(*i == m->x - 1 && *j == m->y - 1)
        *continuer = 0;
    /*int c;
    if (*i == m->y - 1)
    {
        c = *j;
        *j = min(*j + *i + 1, m->x - 1);
        *i = ((m->x - 1) - ((m->y - 1) - c)) + 1;
    }
    else if(*j == 0)
    {
        *j = min(m->y - 1, *i + 1);
        *i = max(0, *i - (m->y - 1) + 1);
    }
    else
    {
        *j = *j - 1;
        *i = *i + 1;
    }*/
    int x = m->x - 1, y = m->y - 1;
    if(x == y)
    {
        if(*i == x)
        {
            *i = *j + 1;
            *j = y;
        }
        else if(*j == 0)
        {
            *j = *i + 1;
            *i = 0;
        }
        else
        {
            *i = *i + 1;
            *j = *j - 1;
        }
    }
    else if(x < y)
    {
        if(*i == x)
        {
            *i = max(*j - (y - x) + 1, 0);
            *j = min(*j + x + 1, y);
        }
        else if(*j == 0)
        {
            *j = *i + 1;
            *i = 0;
        }
        else
        {
            *i = *i + 1;
            *j = *j - 1;
        }
    }
    else if(x > y)
    {
        if(*i == x)
        {
            *i = x - y + *j + 1;
            *j = y;
        }
        else if(*j == 0)
        {
            *j = min(*i + 1, y);
            *i = max(*i - y + 1, 0);
        }
        else
        {
            *i = *i + 1;
            *j = *j - 1;
        }
    }
}

void blitTransparent(SDL_Surface *plat, SDL_Surface *uni, SDL_Rect *R, SDL_Surface *ecran, s_posA *posA, int descente)
/// nouvelle facon d'afficher les cases qui devraient être transparente : on les descend à la hauteur d'affichage du joueur
{

    SDL_Rect r = *R;
    s_posA posa = *posA;
    if(descente < 0)
        descente = 0;
    blit(plat, &r, ecran, &posa);
    r.h = r.h - 39 - (40 * descente);
    posa.y = posa.y + (40 * descente);
    blit(uni, &r, ecran, &posa);

}

void freeMap(s_map *m)
{
    int i;
    for(i = 0;i < m->x;i++)
        free(m->carte[i]);
    free(m->carte);
    SDL_FreeSurface(m->tilesetUni);
    SDL_FreeSurface(m->tilesetTransparent);
    SDL_FreeSurface(m->tilesetPlat);
    SDL_FreeSurface(m->fond);
    free(m);
}



























































































