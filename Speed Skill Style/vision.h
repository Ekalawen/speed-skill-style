
#ifndef VISION_H
#define VISION_H

    #define ANGLE_PLONGEE 30 //par rapport à la vue de dessus totale (qui est 0), 90 étant la vue horizontale, 45 la vue normal
    #define REBORD_DE_VISION 300
    #define AJUSTEMENT_VISION 15 //plus c'est petit, plus c'est rapide

    typedef struct
    {
        s_posA posA;
        double anglePlongee; // à refaire mais en VRAI 3D, cad dans très longtemps
        double zoom; // ça par contre c'est faisable
    }s_vision;

    enum{UNI, TRANSPARENT, PLAT};

    s_vision* initVision();
    void translaterVision(s_vision *v, double x, double y);
    void ajusterVision(s_vision *v, s_joueur *joueur);
    void centrerVision(s_vision *v, s_posV *posV);
    void freeVision(s_vision *v);










#endif















































