
#ifndef FILTRE_H
#define FILTRE_H

    enum{FILTRE_PASSE_HAUT, // met en valeur les détails
         FILTRE_PASSE_BAS, // floute, transforme les diagonales en lignes droites
         FILTRE_MOYENNEUR, // simplifie bcp, ressemble au filtre passe bas
         FILTRE_MOYENNEUR_CENTRAL, // ressemble bcp bcp au filtre moyenneur
         FILTRE_MOYENNEUR_CROIX, // se contente de faire la moyenne uniquement en lignes (pas en diago)
         FILTRE_MOYENNEUR_CENTRAL_CROIX, // pareil mais centré
         FILTRE_LAPLACIEN_1, // effet damier, bcp de trous, terrains chaotique
         FILTRE_LAPLACIEN_2, // inutilisable
         FILTRE_LAPLACIEN_3, // = filtre passe haut
         FILTRE_LAPLACIEN_4, // inutilisable
         FILTRE_LAPLACIEN_5, // exponentialise les écarts
         FILTRE_PREWITT_1, // prewitt et sobels mettent trop d'accent sur l'un des 4 contours
         FILTRE_PREWITT_2,
         FILTRE_SOBEL_1,
         FILTRE_SOBEL_2,
         FILTRE_GRADIENT_HAUT, // non ... juste non
         FILTRE_GRADIENT_BAS,
         FILTRE_GRADIENT_GAUCHE,
         FILTRE_GRADIENT_DROITE,
         FILTRE_GAUSSIEN_T3, // floute aussi, mais en pondérant la distance
         FILTRE_GAUSSIEN_T5, // pareille mais en prenant plus de distance
         };


    double** chargerFiltre(int type, int *taille);
    void appliquerFiltre(s_map *m, SDL_Rect r, int typeFiltre);


#endif // FILTRE_H





















































