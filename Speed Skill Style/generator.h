

#ifndef GENERATOR_H
#define GENERATOR

    // liste de tous les types de terrains
    enum
    {
        MONTAGNE,
        PRECIPICE,
        RIVIERE,
        ILES,
        ILE_AU_TRESOR,
        DESERT,
        LABYRINTHE,
        CAVERNE,
        IMMEUBLES,
        CHAOS,
        DUPLICANTE,
        PLATEAU_ET_ECHELLE,
    };
    #define NB_TERRAIN 12


    s_map* initMap(int x, int y);
    void genMap(s_map *m);
    void genPic(s_map *m, SDL_Rect r, s_posC pic, double *pente);
    void genApplat(s_map *m, SDL_Rect r, int hauteur);
    void genMuraille(s_map *m, SDL_Rect r, int nbMurs, int distMin, int hauteur);
    void genElevator(s_map *m, SDL_Rect r);
    void changerTailleCase(int x, int y, int z, s_map *m);
    void dupliquerMap(s_map *m, int coef);
    void genAccidents(s_map *m, SDL_Rect r, double frequence, int amplitudeMax);
    s_map* genSalle(int x, int y, int hauteurMoyenne);
    void genMontagne(s_map *m, int x, int y, int hauteurMoyenne);
    void genPrecipice(s_map *m, int x, int y, int hauteurMoyenne);
    void genRiviere(s_map *m, int x, int y, int hauteurMoyenne);
    void genIles(s_map *m, int x, int y, int hauteurMoyenne);
    void genIleAuTresor(s_map *m, int x, int y, int hauteurMoyenne);
    void genDesert(s_map *m, int x, int y, int hauteurMoyenne);
    void completerVide(s_map *m, SDL_Rect r, int hauteur);
    s_posC plusProcheVoisinAHauteurFixe(s_map *m, s_posC pos, int hauteur);
    int distanceMaxCoinsHauteur(s_map *m, int hauteur);
    s_map* intersecterMap(s_map *original, s_map *filtre, SDL_Rect r, int valeurIntersectante);
    s_map* exclureMap(s_map *original, s_map *filtre, SDL_Rect r, int valeurExcluante);



#endif // GENERATOR_H
