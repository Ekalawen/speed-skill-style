

#include "include.h"

int main(int argc, char** argv)
{
    SDL_Surface *ecran = NULL;
    int continuer = 1;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();
    SDL_WM_SetCaption("Speed Skill Style", NULL);
    SDL_WM_SetIcon(/*trouver une icone*/NULL, NULL);
    ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    srand(time(NULL));

    s_vision *v = initVision();
    /// ici on peut commenter/décommenter pour passer d'un mode à l'autre
    //s_map *m = chargerMap("niveaux/niveau01");
    s_map *m = initMap(25, 25);
    genMap(m);
    s_joueur *joueur = chargerJoueur(m);
    s_input in;
    memset(&in, 0, sizeof(in));
    SDL_Rect r;
    r.x = 0; r.y = 0;r.w = m->x; r.h = m->y;

    s_posV mvt;
    centrerVision(v, &joueur->pos.v);
    while(continuer)
    {
        updateEvents(&in);

        if(in.key[SDLK_RETURN] || in.key[SDLK_ESCAPE])
            continuer = 0;

        if(in.key[SDLK_z])
            translaterVision(v, 0, 10);
        if(in.key[SDLK_s])
            translaterVision(v, 0, -10);
        if(in.key[SDLK_q])
            translaterVision(v, 10, 0);
        if(in.key[SDLK_d])
            translaterVision(v, -10, 0);

        if(in.key[SDLK_UP])
        {
            mvt.x = -10;
            mvt.y = -10;
            bougerJoueur(joueur, m, &mvt);
        }
        if(in.key[SDLK_DOWN])
        {
            mvt.x = 10;
            mvt.y = 10;
            bougerJoueur(joueur, m, &mvt);
        }
        if(in.key[SDLK_LEFT])
        {
            mvt.x = -10;
            mvt.y = 10;
            bougerJoueur(joueur, m, &mvt);
        }
        if(in.key[SDLK_RIGHT])
        {
            mvt.x = 10;
            mvt.y = -10;
            bougerJoueur(joueur, m, &mvt);
        }
        recupererDirection(&in, joueur);

        afficherMap(m, v, joueur, ecran);
    }

    freeVision(v);
    freeMap(m);
    freeJoueur(joueur);

    TTF_Quit();
    SDL_Quit();
    return EXIT_SUCCESS;
}
















































