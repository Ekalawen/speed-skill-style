

#ifndef MAP_H
#define MAP_H

    #define NB_ETAGE 15

    typedef struct
    {
        s_posTotale pos;
        int etat;
        int affiche;
    }s_infoTile;


    typedef struct
    {
        char *titre;
        int x, y;
        s_infoTile **carte;
        s_posC depart, arrivee;
        SDL_Surface *tilesetUni;
        SDL_Surface *tilesetTransparent;
        SDL_Surface *tilesetPlat;
        SDL_Surface *fond;
    }s_map;

    s_map* chargerMap(char file[]);
    void afficherMap(s_map *m, s_vision *v, s_joueur *joueur, SDL_Surface *ecran);
    void afficherMapAerienne(s_map *m, s_vision *v, s_joueur *joueur, SDL_Surface *ecran);
    void afficherCarteTerminal(s_map *m);
    void parcourirEnDiagonale(int *i, int *j, s_map *m, int *continuer);
    void blitTransparent(SDL_Surface *plat, SDL_Surface *uni, SDL_Rect *R, SDL_Surface *ecran, s_posA *posA, int z);
    void freeMap(s_map *m);

#endif










































