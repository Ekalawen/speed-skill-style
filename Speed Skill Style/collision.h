

#ifndef COLLISION_H
#define COLLISION_H

    /*
    On définit 3 types de positions :
        - s_posC :      //case
            qui sont les positions de l'échiquier, des cases du terrain sur lequel on joue. Ce sont des entiers.
        - s_posV :      //virtuelle
            qui sont les positions virtuelles qui permettent de gérer les collisions, elles sont 80 fois plus importantes
            que les s_posC
        - s_posA :      //affichage
            qui sont les posistions à lesquelles ont affichera les images, elles sont liées par les formules de 2D isométriques
            aux ::
    */


    int collisionJoueurEtage(s_joueur *joueur, s_posV *posV);
    int collisionJoueurAffichageEtage(s_joueur *joueur, s_posTotale *pos);
    int caseVisible(s_posA *posA, int hauteur);
    int collisionAffichageEtage(s_infoTile *caseBasse, s_infoTile *caseHaute);
    int distancePlate(s_posC *case1, s_posC *case2);
    int distancePlateCarre(s_posC *case1, s_posC *case2);
    int poteau(s_posC *case1, s_map *m);
    s_posC* casesOccuppeesParJoueur(s_map *m, s_joueur *joueur);



















#endif













































