


#include "include.h"

s_joueur* chargerJoueur(s_map *m)
{
    s_joueur *joueur = malloc(sizeof(s_joueur));
    joueur->pos.c.x = m->depart.x;
    joueur->pos.c.y = m->depart.y;
    joueur->pos.c.z = m->carte[(int)m->depart.x][(int)m->depart.y].pos.c.z;
    joueur->pos.v.x = m->depart.x * 80;
    joueur->pos.v.y = m->depart.y * 80;
    joueur->pos.v.w = 40;
    joueur->pos.v.h = 40;
    joueur->pos.v.z = joueur->pos.c.z;
    joueur->pos.a = posVconvertPosA(joueur->pos.v);
    ajusterPosAJoueur(joueur);

    joueur->hauteur = m->carte[(int)m->depart.x][(int)m->depart.y].pos.c.z;
    joueur->direction = HAUT; // décision arbitraire mais à paramétrer en fonction de l'arrivé
    joueur->immobile = 1;
    joueur->vitesse = VITESSE;
    joueur->tailleChute = 0;
    joueur->etat = AU_SOL;
    joueur->surface = IMG_Load("images/joueur.png");
    return joueur;
}

void ajusterPosAJoueur(s_joueur *joueur)
{
    joueur->pos.a.x += 20;
    joueur->pos.a.y += -20;
}

s_posC* posAffichageJoueur(s_joueur *joueur, s_map *m)
{
    //recuperer toutes les tiles qui ont un espace d'affichage commun avec le joueur
    //recuperer toutes les cases avec lesquelles le joueur est en contact
    //si zc <= zj alors la case est affiché avant
    //si zc > zj
        //si yc >= yj et xc >= xj (et xc != xj et yc != yj) ==> alors on affiche le joueur juste avant

    /*int i, j;
    s_posC *posC = malloc(sizeof(s_posC));
    posC->z = 0;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(collisionJoueurEtage(joueur, &m->carte[i][j].pos.v) == 1)
            {
                if(m->carte[i][j].pos.c.z >= posC->z)
                {
                    if(collisionJoueurAffichageEtage(joueur, &m->carte[i][j].pos) == 1
                       && m->carte[i][j].pos.c.z > posC->z)
                    {
                        posC->x = m->carte[i][j].pos.c.x;
                        posC->y = m->carte[i][j].pos.c.y;
                        posC->z = m->carte[i][j].pos.c.z;
                        return posC;
                    }
                }
            }
        }
    }
    fprintf(stderr, "\n%d %d", posC->x, posC->y);
    return posC;*/

    int i, j;
    s_posC *posC = malloc(sizeof(s_posC));
    posC->z = -1;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(collisionJoueurEtage(joueur, &m->carte[i][j].pos.v) == 1
               && m->carte[i][j].pos.c.z > posC->z)
            {
                posC->x = m->carte[i][j].pos.c.x;
                posC->y = m->carte[i][j].pos.c.y;
                posC->z = m->carte[i][j].pos.c.z;
            }

        }
    }
    return posC;
}

void freeJoueur(s_joueur *joueur)
{
    SDL_FreeSurface(joueur->surface);
    free(joueur);
}












































