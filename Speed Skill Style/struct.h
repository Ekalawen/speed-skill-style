
// repértorie les structures non-spécifiques à un seul fichier


#ifndef STRUCT_H
#define STRUCT_H

    #define LARGEUR_ECRAN 900
    #define HAUTEUR_ECRAN 900
    #define STR_MAX 1000

    typedef struct
    {
        int x, y, z;
    }s_posC;

    typedef struct
    {
        double x, y, w, h;
        int z;
    }s_posV;

    typedef struct
    {
        int x, y;
    }s_posA;

    typedef struct
    {
        s_posC c;
        s_posV v;
        s_posA a;
        SDL_Rect R;
    }s_posTotale;

    typedef struct
    {
        s_posTotale pos;
        int hauteur;
        int direction;
        int immobile;
        double vitesse;
        int tailleChute;
        int etat;
        SDL_Surface *surface;
    }s_joueur;


#endif















































