

#include "include.h"



void transparenceMap(s_map *m, s_joueur *joueur)
{
    ///une troisième
    //seuls les cases à la droite et en haut du joueur sont opaques
    /*int i, j;
    for(i = 0;i < m->x / 2;i++)
    {
        for(j = 0;j < m->y / 2;j++)
        {
                if((m->carte[2 * i][2 * j].pos.c.x > joueur->pos.c.x
                   &&m->carte[2 * i][2 * j].pos.c.y >= joueur->pos.c.y)
                   ||
                   ((m->carte[2 * i][2 * j].pos.c.x >= joueur->pos.c.x
                   &&m->carte[2 * i][2 * j].pos.c.y > joueur->pos.c.y)))
                {
                   m->carte[2 * i][2 * j].transparence = 1;
                   m->carte[2 * i + 1][2 * j].transparence = 1;
                   m->carte[2 * i][2 * j + 1].transparence = 1;
                   m->carte[2 * i + 1][2 * j + 1].transparence = 1;
                }
                else
                {
                   m->carte[2 * i][2 * j].transparence = 0;
                   m->carte[2 * i + 1][2 * j].transparence = 0;
                   m->carte[2 * i][2 * j + 1].transparence = 0;
                   m->carte[2 * i + 1][2 * j + 1].transparence = 0;
                }
        }
    }*/

    ///une autre qui rend opaque uniquement les étages qui sont compris entre le niveau du joueur et son niveau + X
    /*int i, j, l;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {

            if(m->carte[i][j].pos.c.z > joueur->pos.c.z + PLAFOND_TRANSPARENCE
               ||m->carte[i][j].pos.c.z <= joueur->pos.c.z - SOL_TRANSPARENCE)
                m->carte[i][j].transparence = 1;
            else
                m->carte[i][j].transparence = 0;

            //if(poteau(&m->carte[i][j].pos.c, m) == 1)
            //    m->carte[i][j].transparence = 0;

            for(l = m->carte[i][j].pos.c.z;l > 0;l--)
                if(i - l >= 0 && j - l >= 0)
                    if(collisionAffichageEtage(&m->carte[i][j], &m->carte[i - l][j - l]) == 1)
                        m->carte[i - l][j - l].transparence = 0;
        }
    }*/

    ///une méthode qui rend transparent les cases masquant compètement une autre case (qui est derrière et plus basse)
    /*int i, j, k, l;
    int z;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            z = m->carte[i][j].pos.c.z;
            m->carte[i][j].transparence = 0;
            for(k = i - z;k <= i;k++)
                for(l = j - z;l <= j;l++)
                    if(k >= 0 && l >= 0
                       &&k != i && l != j)
                        if(collisionAffichageEtage(&m->carte[i][j], &m->carte[k][l]) == 1)
                            m->carte[i][j].transparence = 1;
        }
    }*/

    ///cette méthode rend transparent toutes les cases dans les colonnes verticales à moins de X cases du joueur
    /*int i, j, dif;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            dif = (i - joueur->pos.c.x) - (j - joueur->pos.c.y);
            if(dif <= 4 && dif >= -4)
                m->carte[i][j].etat = TRANSPARENT;
            else
                ;
        }
    }*/

    ///cette méthode met tout opaque
    /*int i, j;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0; j < m->y;j++)
        {
            m->carte[i][j].transparence = 0;
        }
    }*/

    ///cette méthode permet de visualiser le joueur si jamais il se retrouve dans un trous
    int i, j;
    int x, y, z, xJ, yJ, zJ, k, dif;
    for(i = 0; i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            x = m->carte[i][j].pos.c.x;
            y = m->carte[i][j].pos.c.y;
            z = m->carte[i][j].pos.c.z;
            xJ = joueur->pos.c.x;
            yJ = joueur->pos.c.y;
            zJ = joueur->pos.c.z;
            k = fabs((x - xJ) - (y - yJ));
            dif = (x - xJ) - (y - yJ);
            if(x >= xJ && y >= yJ && dif <= 3 && dif >= -3)
                m->carte[i][j].etat = TRANSPARENT;
            /*if(x >= xJ && y >= yJ && !(x == xJ && y == yJ) && k <= 2 && z > zJ &&
               m->carte[i][j].etat == UNI && collisionJoueurAffichageEtage(joueur, &m->carte[i][j].pos))
                //if(!(z == zJ + 1))
                m->carte[i][j].etat = TRANSPARENT;*/
        }
    }
}


void devoilerCase(s_map *m, s_joueur *joueur)
/// permet de distinguer les cases suffisamment loin du joueur pour les afficher à plat
{
    int i, j, d;
    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
        {
            d = distancePlate(&m->carte[i][j].pos.c, &joueur->pos.c);

			// TODO
			// a modifier si on veut changer le voile !
            if(d <= DISTANCE_DU_VOILE) {
                m->carte[i][j].etat = PLAT;
			} else {
                m->carte[i][j].etat = UNI;
                //m->carte[i][j].etat = PLAT;
			}
        }

}













































