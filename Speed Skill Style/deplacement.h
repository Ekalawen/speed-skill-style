

#ifndef DEPLACEMENT_H
#define DEPLACEMENT_H

    #define VITESSE 100
    #define CHUTE_GRAVE 4 // 4 ou plus
    #define CHUTE_NORMALE 2 // 2 ou plus
    #define CHUTE_ACCELERE 0  //0 ou plus

    enum{AU_SOL, EN_SAUT, EN_CHUTE, AU_MUR, AGGRIPE, TOURNANT};

    enum{HAUT, BAS, GAUCHE, DROITE, HAUT_GAUCHE, HAUT_DROITE, BAS_GAUCHE, BAS_DROITE};

    void bougerJoueur(s_joueur *joueur, s_map *m, s_posV *mvt);
    void recupererDirection(s_input *in, s_joueur *joueur);

    /*
    tous les mouvements possibles :
        AU_SOL :
            - courir dans les 8 directions (haut, haut droite, droite, droite bas, bas, bas gauche, gauche, gauche haut)
            - si on s'arrête ==> perte de vitesse
            - tourner de 45%
            - tourner de 90%
            - tourner de 135% ou 180% ==> perte de vitesse
            - si on avance et qu'il y a un mur à 90% on s'arrete et ==> perte de vitesse
            - si on avance et qu'il y a un mur à 45% on continue à courrir parallèlement à ce mur
            - si on saute on passe en EN_SAUT devant de 3 cases en long et 1 case en hauteur 0==0
            - si on avance et qu'il n'y a plus de sol à 90% on passe en mode EN_CHUTE au rithme de 1 case de long pour 1 case de haut
            - si on avance et qu'il n'y a plus de sol à 45% on passe en mode AU_MUR
            - pas de lien avec le AGGRIPE
            - pas de lien avec TOURNANT
        EN_SAUT :
            - effectue un mouvement obligatoire de 3 case de longeur de longueur pour 1 case de long
            - pas de possibilité de diriger la direction de son saut en vol
            - on peut repasser en mode CHUTE avant le fin du saut
            - si a finit le mouvement de 3 en longeur pour 1 hauteur on passe en mode EN_CHUTE
            - si on est à la même hauteur qu'un etage en-dessous de nous, on repasse en mode AU_SOL
            - si on atteint un mur à 90% on passe en mode AGGRIPE
            - si on atteint un mur à 45% on passe en mode AU_MUR
            - pas de lien avec TOURNANT
        EN_CHUTE :
            - tombe au rithme de 1 de hauteur pour 1 de longueur
            - pas de possibilité de diriger la direction de la chute
            - pas de possibilité de diriger la fin de la chute
            - si on est 1 case au-dessus d'un etage on passe en mode AU_SOL
                - si on était à moins de 1 case au-dessus de l'étage on gagne de la vitesse
                - si on était entre 2 et 3 case au-dessus de l'étage rien ne se passe
                - si on était à 4 cases ou plus au dessus de l'étage ==> perte de vitesse
            - pas de lien avec EN_SAUT
            - si on arrive sur un mur à 45% on passe en mode AU_MUR
            - si on arrive sur un mur à 90% on passe en mode AGGRIPE
            - pas de lien avec TOURNANT
        AU_MUR :
            - avance le long du mur à une vitesse de course normale
            - reste au mur pendant maximum 3 cases de long (comme le saut) après on passe en mode EN_CHUTE
            - si un étage rejoint notre étage on passe en mode AU_SOL
            - on peut, s'il y a un étage au-dessus de notre course, monter dessus et passer en mode AU_SOL
            - pas de possibilité de dirigier la direction de la course au mur
            - on peut passer en mode EN_SAUT dans la même direction
            - on peut passer en mode EN_SAUT à 45% par rapport au mur
            - on peut passer en mode EN_SAUT à 90% par rapport au mur
            - si on arrive contre un mur à 90% on passe en mode AGGRIPE
            - si le mur s'arrête (lorsque qu'il n'y a plus aucun contact) alors on passe en mode TOURNANT
        AGGRIPE :
            - reste coller au mur pendant 1 sec (ou moins)
            - apres passe en mode CHUTE et ==> perte de vitesse
            - on peut par contre se laisser passer en mode CHUTE volontairement (sans perte de vitesse)
            - on peut aussi monter si l'étage du mur sur lequel on est aggripé au juste un étage au-dessus et on passe en mode AU_SOL
            - on peut passer en mode EN_SAUT parallèlement au mur dans un sens ou l'autre
            - on peut passer en mode EN_SAUT à 90% par rapport au mur
            - on peut passer en mode EN_SAUT à 45% par rapport au mur dans les 2 sens
            - pas de lien avec AU_MUR
            - pas de lien avec TOURNANT
        TOURNANT :
            - fait (monter de 1 case) avec une rotation de 90% et une translation rapide de 1 case sur le coté du mur
            - pas de possibilité de diriger le tournant
            - si il y a un étage en-dessous à la fin de la rotation passe en mode AU_SOL
            - pas de lien avec EN_SAUT (il faut sauter avant ou après, mais c'est un mvt très court de toute façon)
            - repasse en mode AU_MUR à la fin de la rotation
            - pas de lien ave AGGRIPE
        */





#endif








































