


#include "include.h"

void bougerJoueur(s_joueur *joueur, s_map *m, s_posV *mvt)
{
    joueur->pos.v.x += mvt->x;
    joueur->pos.v.y += mvt->y;
    joueur->pos.c = *posAffichageJoueur(joueur, m);
    joueur->pos.v.z = joueur->pos.c.z;
    joueur->pos.a = posVconvertPosA(joueur->pos.v);
    ajusterPosAJoueur(joueur);
}

void recupererDirection(s_input *in, s_joueur *joueur)
{
    int h = in->key[SDLK_UP];
    int b = in->key[SDLK_DOWN];
    int g = in->key[SDLK_LEFT];
    int d = in->key[SDLK_RIGHT];
    joueur->immobile = 0;
    if((h && !b && !g && !d)
       ||(h && !b && g && d))
        joueur->direction = HAUT;
    else if((!h && b && !g && !d)
       ||(!h && b && g && d))
        joueur->direction = BAS;
    else if((!h && !b && g && !d)
       ||(h && b && g && !d))
        joueur->direction = GAUCHE;
    else if((!h && !b && !g && d)
       ||(h && b && !g && d))
        joueur->direction = DROITE;
    else if(h && !b && g && !d)
        joueur->direction = HAUT_GAUCHE;
    else if(h && !b && !g && d)
        joueur->direction = HAUT_DROITE;
    else if(!h && b && g && !d)
        joueur->direction = BAS_GAUCHE;
    else if(!h && b && !g && d)
        joueur->direction = BAS_DROITE;
    else
        joueur->immobile = 1;
}








































