


#include "include.h"



s_vision* initVision()
{
    s_vision *v = malloc(sizeof(s_vision));
    v->posA.x = 0;
    v->posA.y = 0;
    v->anglePlongee = ANGLE_PLONGEE;
    v->zoom = 1;
    return v;
}

void ajusterVision(s_vision *v, s_joueur *joueur)
{
    if(REBORD_DE_VISION > joueur->pos.a.x + (joueur->pos.v.w / 2) + v->posA.x)
        translaterVision(v, (REBORD_DE_VISION - (joueur->pos.a.x + (joueur->pos.v.w / 2) + v->posA.x)) / AJUSTEMENT_VISION, 0);
    if(LARGEUR_ECRAN - REBORD_DE_VISION < joueur->pos.a.x + (joueur->pos.v.w / 2) + v->posA.x)
        translaterVision(v, -((joueur->pos.a.x + (joueur->pos.v.w / 2) + v->posA.x - (LARGEUR_ECRAN - REBORD_DE_VISION))) / AJUSTEMENT_VISION, 0);
    if(REBORD_DE_VISION > joueur->pos.a.y + (joueur->pos.v.h / 2) + v->posA.y)
        translaterVision(v, 0, (REBORD_DE_VISION - (joueur->pos.a.y + (joueur->pos.v.h / 2) + v->posA.y)) / AJUSTEMENT_VISION);
    if(LARGEUR_ECRAN - REBORD_DE_VISION < joueur->pos.a.y + (joueur->pos.v.h / 2) + v->posA.y)
        translaterVision(v, 0, -(joueur->pos.a.y + (joueur->pos.v.h / 2) + v->posA.y - (LARGEUR_ECRAN - REBORD_DE_VISION)) / AJUSTEMENT_VISION);

    if(joueur->immobile != 1)
        switch(joueur->direction)
        {
            case HAUT:
                translaterVision(v, 0, 10);
                break;
            case BAS:
                translaterVision(v, 0, -10);
                break;
            case GAUCHE:
                translaterVision(v, 15, 0);
                break;
            case DROITE:
                translaterVision(v, -15, 0);
                break;
            case HAUT_GAUCHE:
                translaterVision(v, 15, 10);
                break;
            case HAUT_DROITE:
                translaterVision(v, -15, 10);
                break;
            case BAS_GAUCHE:
                translaterVision(v, 15, -10);
                break;
            case BAS_DROITE:
                translaterVision(v, -15, -10);
                break;
        }
}

void centrerVision(s_vision *v, s_posV *posV)
{
    v->posA.x = -40 * ((posV->y - posV->x) / 80) - posV->w;
    v->posA.y = -20 * ((posV->y + posV->x) / 80) + (HAUTEUR_ECRAN / 2) - posV->h;
}

void translaterVision(s_vision *v, double x, double y)
{
    v->posA.x += x;
    v->posA.y += y;
}

void freeVision(s_vision *v)
{
    free(v);
}












































