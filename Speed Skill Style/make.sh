#!/bin/bash

gcc -c collision.c $(sdl-config --cflags --libs)
gcc -c deplacement.c $(sdl-config --cflags --libs)
gcc -c event.c $(sdl-config --cflags --libs)
gcc -c filtre.c $(sdl-config --cflags --libs)
gcc -c generator.c $(sdl-config --cflags --libs)
gcc -c joueur.c $(sdl-config --cflags --libs)
gcc -c labyrinthe.c $(sdl-config --cflags --libs)
gcc -c main.c $(sdl-config --cflags --libs)
gcc -c map.c $(sdl-config --cflags --libs)
gcc -c outil.c $(sdl-config --cflags --libs)
gcc -c transparence.c $(sdl-config --cflags --libs)
gcc -c vision.c $(sdl-config --cflags --libs)

gcc -o sss collision.o deplacement.o event.o filtre.o generator.o joueur.o labyrinthe.o main.o map.o outil.o transparence.o vision.o $(sdl-config --cflags --libs) -lSDL_image -lSDL_ttf -lm

rm collision.o deplacement.o event.o filtre.o generator.o joueur.o labyrinthe.o main.o map.o outil.o transparence.o vision.o
