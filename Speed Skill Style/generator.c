
#include "include.h"

/// il faut refaire les graphismes : traits qui dépassent ou qui sont en commum
/// mettre un socle noir au tileset transparent OK (enfin j'ai trouvé une alternative à la transparence)
/// il faut afficher les informations de la map (nom, taille, nom tilesets, etc ...)
/// ajouter des symboles pour les cases de départ et d'arrivée !

/// pour initier une profondeur de la 3D, on peut rajouter une rotation de 90° de la map : on change la map, pas la vision du joueur !

/// créer une fonction harmoniser qui lisserai les contrastes dans une zone en utilisant les filtres de convolution
/// mettre en place le labyrinthe avec la création de salles aléatoires
/// créer une fonction de jonction pour faire le lien entre les salles et les couloirs


s_map* initMap(int x, int y)
///crée une map vierge
{
    int i, j;
    s_map *m = malloc(sizeof(s_map));
    m->titre = malloc(sizeof(char) * STR_MAX);
    m->titre = "Random Map";
    m->x = x;
    m->y = y;
    m->tilesetUni = IMG_Load("images/tileset_uni.png");
    m->tilesetTransparent = IMG_Load("images/tileset_full_uni_plus_transparent.png");
    m->tilesetPlat = IMG_Load("images/tileset_socle_rectangulaire_uni.png");
    /*m->tilesetUni = IMG_Load("images/tileset_full_uni_ME_blanc.png");
    m->tilesetTransparent = IMG_Load("images/tileset_full_uni_plus_transparent_noir.png");
    m->tilesetPlat = IMG_Load("images/tileset_socle_rectangulaire_uni_ME_blanc.png");*/
    m->fond = IMG_Load("images/matrix01.png");
    m->depart.x = m->x - 1; m->depart.y = m->y - 1;
    m->arrivee.x = 0; m->arrivee.y = 0;

    m->carte = malloc(sizeof(s_infoTile*) * m->x);
    for(i = 0;i < m->x;i++)
        m->carte[i] = malloc(sizeof(s_infoTile) * m->y);

    //initialise la carte à une hauteur de 0
    for(j = 0;j < m->y;j++)
        for(i = 0;i < m->x;i++)
            {
                m->carte[i][j].pos.c.x = i;
                m->carte[i][j].pos.c.y = j;
                m->carte[i][j].pos.c.z = 0;
                m->carte[i][j].pos.v.x = i * 80;
                m->carte[i][j].pos.v.y = j * 80;
                m->carte[i][j].pos.v.w = 80;
                m->carte[i][j].pos.v.h = 80;
                m->carte[i][j].pos.v.z = m->carte[i][j].pos.c.z;
                m->carte[i][j].pos.a = posVconvertPosA(m->carte[i][j].pos.v);

                m->carte[i][j].pos.R.x = 80 * m->carte[i][j].pos.c.z;
                m->carte[i][j].pos.R.y = 0;
                m->carte[i][j].pos.R.w = 80;
                m->carte[i][j].pos.R.h = 80 + m->carte[i][j].pos.c.z * 40;

                m->carte[i][j].etat = UNI;
                m->carte[i][j].affiche = 0;
            }
    return m;
}

void genMap(s_map *m)
/// crée une map entière à partir d'une map vierge
{
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    //*m = *genSalle(m->x, m->y, 8);

    *m = *genSalle(m->x, m->y, 10);
    s_map *m2 = initMap(m->x, m->y);
    genLabyrinthe(m2, r, 2, 1, 0, 0);
    *m = *intersecterMap(m, m2, r, -1);
    dupliquerMap(m, 2);
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
	completerVide(m, r, 0);


    //dupliquerMap(m, 2);
    //completerVide(m, r, 4);
    //genApplat(m, r, 5);
    //genAccidents(m, r, 50.0, 1);
    //dupliquerMap(m, 1);
    //*m = *genSalle(m->x, m->y, 5);
    /*dupliquerMap(m, 3);
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    genAccidents(m, r, 80.0, 3);
    dupliquerMap(m, 3);
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    genAccidents(m, r, 10.0, 1);*/
}

void genPic(s_map *m, SDL_Rect r, s_posC pic, double *pente)
/// ajoute un pic qui se superpose à la map dans la zone r, la pente du pic est pente, l'origine est pic
{
    int i, j, RIEN;
    int a, b, d;
    s_posC posC;
    for(i = r.x;i < r.x + r.w;i++)
        for(j = r.y;j < r.y + r.h;j++)
        {
            a = m->carte[i][j].pos.c.z;
            posC.x = i; posC.y = j; posC.z = a;
            d = distancePlate(&pic, &posC);
            RIEN = directionRelative(pic, posC);
            switch(RIEN)
            {
            case HAUT:
                b = (int) pic.z - (d * pente[HAUT]);
                break;
            case BAS:
                b = (int) pic.z - (d * pente[BAS]);
                break;
            case GAUCHE:
                b = (int) pic.z - (d * pente[GAUCHE]);
                break;
            case DROITE:
                b = (int) pic.z - (d * pente[DROITE]);
                break;
            }
            if(a < b)
            {
                changerTailleCase(i, j, b, m);
            }
        }
}

void genApplat(s_map *m, SDL_Rect r, int hauteur)
/// génère un applat de taille hauteur sur la zone r, cet applat écrase les anciennes valeurs
{
    int i, j;
    for(i = r.x; i < r.x + r.w;i ++)
        for(j = r.y; j < r.y + r.h; j++)
        {
            changerTailleCase(i, j, hauteur, m);
        }
}

void genMuraille(s_map *m, SDL_Rect r, int nbMurs, int distMin, int hauteur)
/// génère une suite de nbMurs murs accolés de taille hauteur dans la zone r (il y aura des murs en diagonale)
{
    int i, j;
    int direction = -1, distance, ancienneDirection = -1;
    s_posC ptDebut, ptFin;
    ptDebut.x = randint(r.x, r.x + r.w);
    ptDebut.y = randint(r.y, r.y + r.h);
    for(i = 0; i < nbMurs; i++)
    {
        while(direction == ancienneDirection
              || direction == directionOpposee(ancienneDirection))
        {
            direction = randint(0, 7);
        }
        switch(direction)
        {
        case HAUT:
            distance = randint(min(distMin, ptDebut.y - r.y), ptDebut.y - r.y);
            ptFin.y = ptDebut.y - distance;
            ptFin.x = ptDebut.x;
            break;
        case BAS:
            distance = randint(min(distMin, r.y + r.h - ptDebut.y), r.y + r.h - ptDebut.y);
            ptFin.y = ptDebut.y + distance;
            ptFin.x = ptDebut.x;
            break;
        case GAUCHE:
            distance = randint(min(distMin, ptDebut.x - r.x), ptDebut.x - r.x);
            ptFin.x = ptDebut.x - distance;
            ptFin.y = ptDebut.y;
            break;
        case DROITE:
            distance = randint(min(distMin, r.x + r.w - ptDebut.x), r.x + r.w - ptDebut.x);
            ptFin.x = ptDebut.x + distance;
            ptFin.y = ptDebut.y;
            break;
        case HAUT_GAUCHE:
            distance = randint(min(distMin, min(ptDebut.y - r.y, ptDebut.x - r.x)), min(ptDebut.y - r.y, ptDebut.x - r.x));
            ptFin.x = ptDebut.x - distance;
            ptFin.y = ptDebut.y - distance;
            break;
        case HAUT_DROITE:
            distance = randint(min(distMin, min(ptDebut.y - r.y, r.x + r.w - ptDebut.x)), min(ptDebut.y - r.y, r.x + r.w - ptDebut.x));
            ptFin.x = ptDebut.x + distance;
            ptFin.y = ptDebut.y - distance;
            break;
        case BAS_GAUCHE:
            distance = randint(min(distMin, min(r.y + r.h - ptDebut.y, ptDebut.x - r.x)), min(r.y + r.h - ptDebut.y, ptDebut.x - r.x));
            ptFin.x = ptDebut.x - distance;
            ptFin.y = ptDebut.y + distance;
            break;
        case BAS_DROITE:
            distance = randint(min(distMin, min(r.y + r.h - ptDebut.y, r.x + r.w - ptDebut.x)), min(r.y + r.h - ptDebut.y, r.x + r.w - ptDebut.x));
            ptFin.x = ptDebut.x + distance;
            ptFin.y = ptDebut.y + distance;
            break;
        }
        for(j = 0;j < distance;j ++)
        {
            switch(direction)
            {
            case HAUT:
                changerTailleCase(ptDebut.x, ptDebut.y - j, hauteur, m);
                break;
            case BAS:
                changerTailleCase(ptDebut.x, ptDebut.y + j, hauteur, m);
                break;
            case GAUCHE:
                changerTailleCase(ptDebut.x - j, ptDebut.y, hauteur, m);
                break;
            case DROITE:
                changerTailleCase(ptDebut.x + j, ptDebut.y, hauteur, m);
                break;
            case HAUT_GAUCHE:
                changerTailleCase(ptDebut.x - j, ptDebut.y - j, hauteur, m);
                break;
            case HAUT_DROITE:
                changerTailleCase(ptDebut.x + j, ptDebut.y - j, hauteur, m);
                break;
            case BAS_GAUCHE:
                changerTailleCase(ptDebut.x - j, ptDebut.y + j, hauteur, m);
                break;
            case BAS_DROITE:
                changerTailleCase(ptDebut.x + j, ptDebut.y + j, hauteur, m);
                break;
            }
        }
        if(distance == 0)
            i--;
        ptDebut.x = ptFin.x;
        ptDebut.y= ptFin.y;
        ancienneDirection = direction;
    }
}

void genElevator(s_map *m, SDL_Rect r)
/// génère une suite de case qui permettent de passer de l'étage 0 à l'étage 14 très facilement dans la zone r
{
    int i, j, x, y, z;
    int direction, continuer = 1;
    s_posC pos, voisin;
    SDL_Rect r2;

    s_map *carte = initMap(r.w, r.h);
    for(i = 0;i < r.w;i++)
        for(j = 0;j < r.h;j++)
            changerTailleCase(i, j, -1, carte);
    r2.x = 0; r2.y = 0; r2.w = r.w; r2.h = r.h;

    x = randint(0, r.w - 1);
    y = randint(0, r.h - 1);
    s_maillon *actuel = initMaillon(x, y, carte, r, 1);
    z = 0;
    changerTailleCase(actuel->x, actuel->y, 1, carte);
    changerTailleCase(r.x + actuel->x, r.y + actuel->y, z, m);


    while(z < 14 && continuer == 1)
    {
        majPossibiliteMaillon(actuel, carte, r2, 1);
        if(possibilitesMaillon(actuel) != 0)
        {
            do
            {
                direction = randint(0, 3);
            }while(actuel->possibilites[direction] == 0);

            switch(direction)
            {
            case HAUT:
                actuel->y -= 1;
                break;
            case BAS:
                actuel->y += 1;
                break;
            case GAUCHE:
                actuel->x -= 1;
                break;
            case DROITE:
                actuel->x += 1;
                break;
            default:
                fprintf(stderr, "\nIl y a eus une erreur dans le choix de la direction dans genElevator");
                break;
            }

            changerTailleCase(actuel->x, actuel->y, 1, carte);
            z ++;
            changerTailleCase(r.x + actuel->x, r.y + actuel->y, z, m);
        }
        else
        {
            pos.x = actuel->x; pos.y = actuel->y;
            voisin = plusProcheVoisinAHauteurFixe(carte, pos, -1);
            actuel->x = voisin.x;
            actuel->y = voisin.y;

            changerTailleCase(actuel->x, actuel->y, 1, carte);
            z ++;
            changerTailleCase(r.x + actuel->x, r.y + actuel->y, z, m);
        }

        continuer = 0;
        for(i = 0;i < r.w;i++)
        {
            for(j = 0;j < r.h;j++)
                if(carte->carte[i][j].pos.c.z == -1)
                {
                    continuer = 1;
                    break;
                }
            if(continuer = 1)
                break;
        }
    }
}

void changerTailleCase(int x, int y, int z, s_map *m)
/// change la hauteur d'une case
{
    m->carte[x][y].pos.c.z = z;
    m->carte[x][y].pos.v.z = z;
    m->carte[x][y].pos.a = posVconvertPosA(m->carte[x][y].pos.v);
    m->carte[x][y].pos.R.x = 80 * z;
    m->carte[x][y].pos.R.h = 80 + z * 40;
}

void dupliquerMap(s_map *m, int coef)
///prend la map et la multiplie par coef, chaque case représentera donc un carré de cases de coté coef
{
    int i, j;
    int newX = coef * m->x, newY = coef * m->y;
    s_map *newM = initMap(newX, newY);
    for(i = 0; i < newX; i++)
    {
        for(j = 0; j < newY;j++)
        {
            newM->carte[i][j].pos.c.x = i;
            newM->carte[i][j].pos.c.y = j;
            newM->carte[i][j].pos.c.z = m->carte[i / coef][j / coef].pos.c.z;

            newM->carte[i][j].pos.v.x = i * 80;
            newM->carte[i][j].pos.v.y = j * 80;
            newM->carte[i][j].pos.v.w = 80;
            newM->carte[i][j].pos.v.h = 80;
            newM->carte[i][j].pos.v.z = newM->carte[i][j].pos.c.z;

            newM->carte[i][j].pos.a = posVconvertPosA(newM->carte[i][j].pos.v);

            newM->carte[i][j].pos.R.x = 80 * newM->carte[i][j].pos.c.z;
            newM->carte[i][j].pos.R.y = 0;
            newM->carte[i][j].pos.R.w = 80;
            newM->carte[i][j].pos.R.h = 80 + newM->carte[i][j].pos.c.z * 40;

            newM->carte[i][j].etat = UNI;
            newM->carte[i][j].affiche = 0;
        }
    }
    m->carte = realloc(m->carte, sizeof(s_infoTile*) * newX);
    if(m->carte == NULL)
        fprintf(stderr, "\nGrosse erreur de la mort ...");
    for(i = 0; i < m->x;i++)
        m->carte[i] = realloc(m->carte[i], sizeof(s_infoTile) * newY);
    for(i = m->x; i < newX;i++)
        m->carte[i] = malloc(sizeof(s_infoTile) * newY);

    m->x = newX;
    m->y = newY;
    m->depart.x = m->depart.x * coef;
    m->depart.y = m->depart.y * coef;
    for(i = 0; i < newX;i++)
    {
        for(j = 0;j < newY;j++)
        {
            m->carte[i][j] = newM->carte[i][j];
        }
    }
    freeMap(newM);
}

void genAccidents(s_map *m, SDL_Rect r, double frequence, int amplitudeMax)
/// rajoute des petits pics ou des petits trous aléatoires dans la zone r de hauteur variable entre 0 et amplitudeMax à la frequence frequence
{
    int i, j;
    int h;
    for(i = r.x;i < r.x + r.w;i++)
    {
        for(j = r.y; j < r.y + r.h;j++)
        {
            if(randdouble(0, 100) < frequence)
            {
                do
                {
                    h = m->carte[i][j].pos.c.z + randint(min(0, amplitudeMax), max(0, amplitudeMax));
                }while(h == m->carte[i][j].pos.c.z);
                if(h > 14)
                    h = 14;
                changerTailleCase(i, j, h, m);
            }
        }
    }
}

s_map* genSalle(int x, int y, int hauteurMoyenne)
/// génère une salle aléatoire parmis tous les types de reliefs possibles, ces salles sont faites pour être incorporés dans la map
{
    s_map *m = initMap(x, y);
    //int type = randint(0, NB_TERRAIN - 1);
    int type = MONTAGNE;


    switch(type)
    {
    case MONTAGNE:
        // génère une unique montagne au centre avec une pente pour chaque coté en fonction de sa distance au coté
        // condition : petite map
        genMontagne(m, x, y, hauteurMoyenne);
        break;
    case PRECIPICE:
        // génère un immense trou qui peut être vide ou juste profond, les bords seront à la hauteur demandé
        // condition : une hauteur moyenne suffisememnt (pas une map trop petite non plus)
        genPrecipice(m, x, y, hauteurMoyenne);
        break;
    case RIVIERE:
        // génère une vallée escorté par deux remontée parallèles dans une certaine direction
        // condition : une hauteur moyenne suffisement élevé (mais moins)
        genRiviere(m, x, y, hauteurMoyenne);
        break;
    case ILES:
        // génère un certain nombre de petites parcelles de terres plus ou moins abruptes, avec du vide les séparant
        // condition : une hauteur moyenne pas trop élevé
        genIles(m, x, y, hauteurMoyenne);
        break;
    case ILE_AU_TRESOR:
        // génère une montagne escarpé, entouré de vide, et qui possède un escalator caché en son centre !
        // condition : une hauteur moyenne pas trop élevé (mais moins)
        genIleAuTresor(m, x, y, hauteurMoyenne);
        break;
    case DESERT:
        // génère un espace relativement plat sur 2 ou 3 niveaux, avec des pics parsemés de part en part
        // condition : graaaande map
        genDesert(m, x, y, hauteurMoyenne);
        break;
    case LABYRINTHE:
        // génère un labyrinthe, de pas, de répétition, et de profondeur variable
        // condition : une hauteur moyenne pas trop élevé
        break;
    case CAVERNE:
        // génère un ensemble caverneux, qui proviendra d'une autre type de relief
        // condition : aucune
        break;
    case IMMEUBLES:
        // génère un ensemble de pics extrèmement abruptes très ressérés séparés par un sol très bas
        // condition : une hauteur moyenne très basse
        break;
    case CHAOS:
        // génère un gros bordel volontairement inextricable à une hauteur moyenne fixe
        // condition : aucune
        break;
    case DUPLICANTE:
        // génère aléatoirement un des autres reliefs sur une taille plus petite, avant de répéter le motif dans toutes les directions
        // condition : aucune
        break;
    case PLATEAU_ET_ECHELLE:
        // génère un ensemble de terrains rectangulaires quasiment plats de hauteur éloignés avec des blocs pour permettre de remonter
        // condition : aucune
        break;
    default:
        fprintf(stderr, "\nType de terrain inexistant");
        break;
    }
    return m;
}

void genMontagne(s_map *m, int x, int y, int hauteurMoyenne)
/// génère une montagne
{
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = x; r.h = y;
    s_posC pic;
    double p[4];
    pic.x = randint(x / 4, 3 * x / 4);
    pic.y = randint(y / 4, 3 * y / 4);
    pic.z = randint(min(hauteurMoyenne + 2, 14), 14);
    p[HAUT] = (double) (pic.z - hauteurMoyenne) / (pic.y * 1);
    p[BAS] = (double) (pic.z - hauteurMoyenne) / ((y - pic.y) * 1);
    p[GAUCHE] = (double) (pic.z - hauteurMoyenne) / (pic.x * 1);
    p[DROITE] = (double) (pic.z - hauteurMoyenne) / ((x - pic.x) * 1);
    genPic(m, r, pic, p);
}

void genPrecipice(s_map *m, int x, int y, int hauteurMoyenne)
/// génère un précipice
{
    int i, j;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = x; r.h = y;
    s_posC pic;
    double p[4];
    double freq;
    int epaisseur;
    s_posC centre;
    freq = 25;;
    epaisseur = x / randint(5, 7);
    centre.x = x / 2;
    centre.y = y / 2;
    for(i = 0; i < x;i++)
    {
        for(j = 0;j < y;j++)
        {
            if((i < epaisseur || i >= x - epaisseur)
                ||(j < epaisseur || j >= y - epaisseur))
            {
                pic.x = i;
                pic.y = j;
                pic.z = hauteurMoyenne;
                if(randdouble(0, 100) < freq)
                {
                    switch(directionRelative(centre, pic))
                    {
                    case HAUT:
                        p[HAUT] = randdouble(0, 1.5); p[BAS] = randdouble(3, 7); p[GAUCHE] = randdouble(1, 2.5); p[DROITE] = randdouble(1, 2.5);
                        genPic(m, r, pic, p);
                        break;
                    case BAS:
                        p[HAUT] = randdouble(3, 7); p[BAS] = randdouble(0, 1.5); p[GAUCHE] = randdouble(1, 2.5); p[DROITE] = randdouble(1, 2.5);
                        genPic(m, r, pic, p);
                        break;
                    case GAUCHE:
                        p[HAUT] = randdouble(1, 2.5); p[BAS] = randdouble(1, 2.5); p[GAUCHE] = randdouble(0, 1.5); p[DROITE] = randdouble(3, 7);
                        genPic(m, r, pic, p);
                        break;
                    case DROITE:
                        p[HAUT] = randdouble(1, 2.5); p[BAS] = randdouble(1, 2.5); p[GAUCHE] = randdouble(3, 7); p[DROITE] = randdouble(0, 1.5);
                        genPic(m, r, pic, p);
                        break;
                    default:
                        fprintf(stderr, "\nErreur lors de la formation d'un précipice");
                        break;
                    }
                }
            }
        }
    }
    freq = randdouble(5, 15);
    for(i = 0;i < x;i++)
    {
        for(j = 0;j < y;j++)
        {
            if(m->carte[i][j].pos.c.z == 0)
            {
                if(randdouble(0, 100) < freq)
                    changerTailleCase(i, j, randint(0, 2), m);
                else
                    changerTailleCase(i, j, -1, m);
            }
        }
    }

    r.x = 0; r.y = 0; r.w = m->x - epaisseur; r.h = epaisseur;
    appliquerFiltre(m, r, FILTRE_GAUSSIEN_T3);

    r.x = m->x - epaisseur; r.y = 0; r.w = epaisseur; r.h = m->y - epaisseur;
    appliquerFiltre(m, r, FILTRE_GAUSSIEN_T3);

    r.x = epaisseur; r.y = m->y - epaisseur; r.w = m->x - epaisseur; r.h = epaisseur;
    appliquerFiltre(m, r, FILTRE_GAUSSIEN_T3);

    r.x = 0; r.y = epaisseur; r.w = epaisseur; r.h = m->y - epaisseur;
    appliquerFiltre(m, r, FILTRE_GAUSSIEN_T3);
}

void genRiviere(s_map *m, int x, int y, int hauteurMoyenne)
/// génère une rivière
{
    int i, j, k;
    SDL_Rect r;
    s_posC pos;
    s_posC voisin;
    int profondeur = randint(0, 2);
    int propagation = randint(1, 4);
    int distance, h;

    int **carte = malloc(sizeof(int*) * m->x);
    for(i = 0;i < m->x;i++)
        carte[i] = malloc(sizeof(int) * m->y);
    for(i = 0;i < m->x;i ++)
        for(j = 0;j < m->y;j++)
            carte[i][j] = 0;

    r.x = 0; r.y = 0; r.w = x; r.h = y;
    genApplat(m, r, -1);
    r.w = x - 1; r.h = y - 1;
    genMuraille(m, r, randint(5, 8), (x + y) / 4, profondeur);
    r.x = 0; r.y = 0; r.w = x; r.h = y;
    pos.z = hauteurMoyenne;

    for(k = 0; k < propagation;k++)
    {
        for(i = 0;i < x;i++)
        {
            for(j = 0; j < y;j++)
            {
                if(m->carte[i][j].pos.c.z != profondeur)
                {
                    pos.x = i; pos.y = j;
                    voisin = plusProcheVoisinAHauteurFixe(m, pos, profondeur);
                    distance = distancePlate(&pos, &voisin);
                    if(distance == 1
                       && randdouble(0, 100) < 35.0)
                    {
                        carte[i][j] = 1;
                    }
                }
            }
        }
        for(i = 0;i < x;i++)
            for(j = 0;j < y;j++)
                if(carte[i][j] == 1)
                    changerTailleCase(i, j, profondeur, m);
    }

    for(i = 0; i < x;i ++)
    {
        for(j = 0;j < y;j++)
        {
            if(m->carte[i][j].pos.c.z != profondeur)
            {
                pos.x = i; pos.y = j; pos.z = hauteurMoyenne;
                voisin = plusProcheVoisinAHauteurFixe(m, pos, profondeur);
                distance = distancePlate(&pos, &voisin);
                distance += profondeur;
                h = min(distance, hauteurMoyenne);
                if(h == hauteurMoyenne)
                    h += randint(0, 1);
                changerTailleCase(i, j, h, m);

            }
        }
    }
}

void genIles(s_map *m, int x, int y, int hauteurMoyenne)
/// génère des iles
{
    int i, j, k;
    double freq = 6 - hauteurMoyenne;
    int nbIteration = 100, nbMursProches, nbMursLoins, distance;
    int h;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = x; r.h = y;
    s_posC pos, voisin;
    double p[4];

    int **carte = malloc(sizeof(int*) * m->x);
    for(i = 0;i < m->x;i++)
        carte[i] = malloc(sizeof(int) * m->y);

    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            changerTailleCase(i, j, -1, m);

    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(randdouble(0, 100) < freq)
            {
                pos.x = i; pos.y =j;
                pos.z = randint(0, hauteurMoyenne);
                for(k = 0;k < 4;k++)
                    p[k] = randdouble(1, 1);
                genPic(m, r, pos, &p);
            }
        }
    }

    freq = randdouble(2, 12);
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(m->carte[i][j].pos.c.z == -1)
            {
                if(randdouble(0, 100) < freq)
                    changerTailleCase(i, j, randint(0, 1), m);
            }

        }
    }




    /*for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            changerTailleCase(i, j, randint(-1, 0), m);

    for(k = 0;k < nbIteration;k++)
    {
        for(i = 0;i < m->x;i++)
        {
            for(j = 0;j < m->y;j++)
            {
                nbMursProches = compterHauteur(m, i, j, 1, -1);
                nbMursLoins = compterHauteur(m, i, j, 2, -1);
                if(nbMursProches >= 6)
                    carte[i][j] = -1;
                else if(nbMursLoins <= 3)
                    carte[i][j] = -1;
                else
                    carte[i][j] = 0;
            }
        }
        for(i = 0;i < m->x;i++)
            for(j = 0;j < m->y;j++)
                changerTailleCase(i, j, carte[i][j], m);
    }
    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            if(m->carte[i][j].pos.c.z == -1)
                changerTailleCase(i, j, 0, m);
            else
                changerTailleCase(i, j, -1, m);

    freq = 15;
    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
        {
            if(m->carte[i][j].pos.c.z != -1)
            {
                pos.x = i; pos.y = j;
                voisin = plusProcheVoisinAHauteurFixe(m, pos, -1);
                distance = distancePlate(&pos, &voisin);
                distance = min(distance - 1, 14);
                if(distance < hauteurMoyenne && randdouble(0, 100) < freq)
                    distance += randint(1, hauteurMoyenne - distance);
                changerTailleCase(i, j, distance, m);
            }
        }*/
}

void genIleAuTresor(s_map *m, int x, int y, int hauteurMoyenne)
{
    int i, j, max = 0, xmax = 0, ymax = 0, nbmax = 0, nbAdjacent;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    s_posC pic, pos, voisin;
    pic.x = randint(3 * x / 8, 5 * x / 8);
    pic.y = randint(3 * y / 8, 5 * y / 8);
    pic.z = hauteurMoyenne;
    double p[4];
    p[HAUT] = (double) (pic.z + randint(m->y / 8, m->y / 5)) / (pic.y);
    p[BAS] = (double) (pic.z + randint(m->y / 8, m->y / 5)) / (m->y - pic.y);
    p[GAUCHE] = (double) (pic.z + randint(m->x / 8, m->x / 5)) / (pic.x);
    p[DROITE] = (double) (pic.z + randint(m->x / 8, m->x / 5)) / (m->x - pic.x);

    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            changerTailleCase(i, j, -1, m);

    genPic(m, r, pic, p);


    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            if(m->carte[i][j].pos.c.z > max)
                max = m->carte[i][j].pos.c.z;

    for(i = 0;i < m->x;i++)
        for(j = 0;j < m->y;j++)
            if(m->carte[i][j].pos.c.z == max)
            {
                xmax += i;
                ymax += j;
                nbmax ++;
            }
    xmax = xmax / nbmax;
    ymax = ymax / nbmax;

    r.x = xmax - randint(4, 8);
    r.y = ymax - randint(4, 8);
    r.w = randint(9, 17);
    r.h = randint(9, 17);
    genAccidents(m, r, 15, 2);

    r.x = xmax - randint(1, 2);
    r.y = ymax - randint(1, 2);
    r.w = randint(4, 5);
    r.h = randint(4, 5);
    genElevator(m, r);


    pos.x = xmax; pos.y = ymax;
    voisin = plusProcheVoisinAHauteurFixe(m, pos, 0);

    s_map *m2 = initMap(x, y);
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    genLabyrinthe(m2, r, 4, 1, voisin.x, voisin.y);
    *m = *exclureMap(m, m2, r, -1);

    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(m->carte[i][j].pos.c.z == -1)
            {
                nbAdjacent = 0;
                if(caseVide(i - 1, j, m, r) != EN_DEHORS
                   &&m->carte[i - 1][j].pos.c.z >= 1)
                    nbAdjacent++;
                if(caseVide(i + 1, j, m, r) != EN_DEHORS
                   &&m->carte[i + 1][j].pos.c.z >= 1)
                    nbAdjacent++;
                if(caseVide(i, j - 1, m, r) != EN_DEHORS
                   &&m->carte[i][j - 1].pos.c.z >= 1)
                    nbAdjacent++;
                if(caseVide(i, j + 1, m, r) != EN_DEHORS
                   &&m->carte[i][j + 1].pos.c.z >= 1)
                    nbAdjacent++;

                if(nbAdjacent >= 1)
                    changerTailleCase(i, j, 0, m);
            }
        }
    }
}

void genDesert(s_map *m, int x, int y, int hauteurMoyenne)
{
    int i, j, w, h;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            if(randdouble(0, 100) < 50.0)
                changerTailleCase(i, j, max(hauteurMoyenne - 1, -1), m);
            else
                changerTailleCase(i, j, hauteurMoyenne, m);
        }
    }
    appliquerFiltre(m, r, FILTRE_GAUSSIEN_T3);

    // créer une galerie
    if(randdouble(0, 100) < 25)
    {
        s_map *m2 = initMap(x, y);
        genLabyrinthe(m2, r, randint(3, 5), 1, randint(0, m->x - 1), randint(0, m->y - 1));
        *m = *exclureMap(m, m2, r, -1);
        completerVide(m, r, max(hauteurMoyenne - 2, -1));
    }

    do
    {
        w = randint(3, 5);
        h = randint(3, 5);
    }while(w * h < 15);
    fprintf(stderr, "\nw %d h %d", w, h);
    r.w = w;
    r.h = h;
    r.x = randint(0, m->x - 1 - r.w);
    r.y = randint(0, m->y - 1 - r.h);
    genElevator(m, r);
}

void completerVide(s_map *m, SDL_Rect r, int hauteur)
/// remplace le vide (-1) d'une map par des cases de taille hauteur
{
    int i, j;
    for(i = r.x;i < r.x + r.w;i++)
    {
        for(j = r.y; j < r.y + r.h;j++)
        {
            if(m->carte[i][j].pos.c.z == -1)
                changerTailleCase(i, j, hauteur, m);
        }
    }
}

s_posC plusProcheVoisinAHauteurFixe(s_map *m, s_posC pos, int hauteur)
/// renvoie la position de la case la plus proche de taille hauteur de pos (utilise un algorithme de parcours en largeur BFS)
{
    int i, j, x, y;
    int **carte = malloc(sizeof(int*) * m->x);
    for(i = 0;i < m->x;i++)
        carte[i] = malloc(sizeof(int) * m->y);
    for(i = 0;i < m->x;i ++)
        for(j = 0;j < m->y;j++)
            carte[i][j] = 0;
    carte[pos.x][pos.y] = 1;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;
    s_posC retour;

    s_maillon *file = malloc(sizeof(s_maillon));
    file->x = pos.x; file->y = pos.y;
    file->precedant = NULL;
    file->suivant = NULL;

    s_maillon *actuel = malloc(sizeof(s_maillon));

    while(fileEstVide(file) != 1)
    {
        actuel = defiler(file);
        x = actuel->x; y = actuel->y;

        if(m->carte[x][y].pos.c.z == hauteur)
        {
            retour.x = x; retour.y = y; retour.z = hauteur;
            return retour;
        }

        if(caseVide(x - 1, y, m, r) != EN_DEHORS
           && carte[x - 1][y] != 1)
        {
            enfiler(file, x - 1, y);
            carte[x - 1][y] = 1;
        }
        if(caseVide(x + 1, y, m, r) != EN_DEHORS
           && carte[x + 1][y] != 1)
        {
            enfiler(file, x + 1, y);
            carte[x + 1][y] = 1;
        }
        if(caseVide(x, y - 1, m, r) != EN_DEHORS
           && carte[x][y - 1] != 1)
        {
            enfiler(file, x, y - 1);
            carte[x][y - 1] = 1;
        }
        if(caseVide(x, y + 1, m, r) != EN_DEHORS
           && carte[x][y + 1] != 1)
        {
            enfiler(file, x, y + 1);
            carte[x][y + 1] = 1;
        }
        //afficherFile(file);
    }
    fprintf(stderr, "\nOn a pas trouvé de case de cette hauteur !");
    exit(EXIT_FAILURE);
}

int distanceMaxCoinsHauteur(s_map *m, int hauteur)
/// renvoie la distance maximale des 4 coins à une taille hauteur
{
    s_posC pos, voisin;
    int max = 0;
    int a;
    pos.x = 0; pos.y = 0;
    voisin = plusProcheVoisinAHauteurFixe(m, pos, hauteur);
    a = distancePlate(&pos, &voisin);
    if(a > max)
        max = a;
    pos.x = m->x - 1; pos.y = 0;
    voisin = plusProcheVoisinAHauteurFixe(m, pos, hauteur);
    a = distancePlate(&pos, &voisin);
    if(a > max)
        max = a;
    pos.x = 0; pos.y = m->y - 1;
    voisin = plusProcheVoisinAHauteurFixe(m, pos, hauteur);
    a = distancePlate(&pos, &voisin);
    if(a > max)
        max = a;
    pos.x = m->x - 1; pos.y = m->y - 1;
    voisin = plusProcheVoisinAHauteurFixe(m, pos, hauteur);
    a = distancePlate(&pos, &voisin);
    if(a > max)
        max = a;
    return max;
}

int compterHauteur(s_map *m, int x, int y, int distance, int hauteur)
/// compte le nombre de case de taille hauteur dans un perimètre distance autour du point (x, y)
{
    int i, j;
    int nb = 0;
    SDL_Rect r;
    r.x = 0; r.y = 0; r.w = m->x; r.h = m->y;

    for(i = -distance;i < distance + 1;i++)
    {
        for(j = -distance;j < distance + 1;j++)
        {
            if(caseVide(x + i, y + j, m, r) != EN_DEHORS
               && m->carte[x + i][y + j].pos.c.z == hauteur)
            nb ++;
        }
    }
    return nb;
}

s_map* intersecterMap(s_map *original, s_map *filtre, SDL_Rect r, int valeurIntersectante)
/// fusionne les maps original et filtre, en prenant la valeur de original tout le temps sauf lorsque filtre vaut valeurIntersectante, dans ce cas on vide la case, le tout dans la zone r
{
    s_map *m = initMap(original->x, original->y);
    int i, j;
    for(i = 0; i < original->x;i++)
    {
        for(j = 0; j < original->y;j++)
        {
            if(i >= r.x && i < r.x + r.w
               &&j >= r.y && j < r.y + r.h)
            {
                if(filtre->carte[i - r.x][j - r.y].pos.c.z == valeurIntersectante)
                    changerTailleCase(i, j, -1, m);
                else
                    changerTailleCase(i, j, original->carte[i][j].pos.c.z, m);
            }
            else
                changerTailleCase(i, j, original->carte[i][j].pos.c.z, m);
        }
    }
    return m;
}

s_map* exclureMap(s_map *original, s_map *filtre, SDL_Rect r, int valeurExcluante)
/// fusionne 2 map, en ne gardant de la première que les valeurs dont la hauteur sur la deuxième valent valeurExcluante
{
    s_map *m = initMap(original->x, original->y);
    int i, j;
    for(i = 0;i < original->x;i++)
    {
        for(j = 0;j < original->y;j++)
        {
            if(i >= r.x && i < r.x + r.w
               && j >= r.y && j < r.y + r.h)
            {
                if(filtre->carte[i - r.x][j - r.y].pos.c.z == valeurExcluante)
                    changerTailleCase(i, j, original->carte[i][j].pos.c.z, m);
                else
                    changerTailleCase(i, j, -1, m);
            }
            else
                changerTailleCase(i, j, original->carte[i][j].pos.c.z, m);
        }
    }
    return m;
}
























































