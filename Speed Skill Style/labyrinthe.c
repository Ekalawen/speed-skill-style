

#include "include.h"

void genLabyrinthe(s_map *m, SDL_Rect r, int pas, int repetition, int xdebut, int ydebut)
/// crée un labyrinthe plat
{
    /// initialiser la hauteur de la carte à -1 (qui symbolise le vide)
    /// choisir un point de départ
    /// tant qu'il reste des possibilités à tous les maillons
        /// choisir une direction
        /// avancer d'un pas
        /// ériger tous les nouveaux maillons engendrés
        /// mettre les nouvelles valeurs à 0

    // initialisation de la carte à vide
    int i, j;
    for(i = r.x;i < r.x + r.w;i++)
    {
        for(j = r.y;j < r.y + r.h;j++)
        {
            changerTailleCase(i, j, -1, m);
        }
    }

    // choix du point de départ
    //int x = randint(r.x, r.x + r.w - 1), y = randint(r.y, r.y + r.h - 1);
    int x = xdebut, y = ydebut;
    s_maillon *maillonActuel = initMaillon(x, y, m, r, pas * repetition);
    s_maillon *newMaillon;
    changerTailleCase(x, y, 3, m);

    // création du labyrinthe
    int direction;
    while(restePossibilites(maillonActuel))
    {
        majPossibiliteMaillon(maillonActuel, m, r, pas * repetition);
        if(possibilitesMaillon(maillonActuel) != 0)
        {
            do
            {
                direction = randint(0, 3);
            }while(maillonActuel->possibilites[direction] == 0);

            for(j = 0;j < repetition;j++)
            {
                switch(direction)
                {
                case HAUT:
                    newMaillon = initMaillon(x, y - pas, m, r, pas);
                    for(i = 1;i < pas;i++)
                        changerTailleCase(x, y - i, 1, m);
                    break;
                case BAS:
                    newMaillon = initMaillon(x, y + pas, m, r, pas);
                    for(i = 1;i < pas;i++)
                        changerTailleCase(x, y + i, 1, m);
                    break;
                case GAUCHE:
                    newMaillon = initMaillon(x - pas, y, m, r, pas);
                    for(i = 1;i < pas;i++)
                        changerTailleCase(x - i, y, 1, m);
                    break;
                case DROITE:
                    newMaillon = initMaillon(x + pas, y, m, r, pas);
                    for(i = 1;i < pas;i++)
                        changerTailleCase(x + i, y, 1, m);
                    break;
                }
                ajouterMaillonEnQueue(maillonActuel, newMaillon);
                maillonActuel = newMaillon;
                x = maillonActuel->x;
                y = maillonActuel->y;
                changerTailleCase(x, y, 1, m);
            }
        }
        else
        {
            if(maillonActuel->precedant != NULL)
            {
                maillonActuel = maillonActuel->precedant;
                x = maillonActuel->x;
                y = maillonActuel->y;
            }
        }
    }
	//completerVide(m, r, 2);
}

s_maillon* initMaillon(int x, int y, s_map *m, SDL_Rect r, int pas)
/// initialise un maillon
{
    s_maillon *maillon = malloc(sizeof(*maillon));
    maillon->x = x;
    maillon->y = y;
    maillon->precedant = NULL;
    maillon->suivant = NULL;
    majPossibiliteMaillon(maillon, m, r, pas);
    return maillon;
}

int premierMaillon(s_maillon *maillon)
/// dit si c'est le premier maillon de la chaine
{
    if(maillon->precedant == NULL)
        return 1;
    return 0;
}

int dernierMaillon(s_maillon *maillon)
/// dit si c'est le dernier maillon de la chaine
{
    if(maillon->suivant == NULL)
        return 1;
    return 0;
}

void majPossibiliteMaillon(s_maillon *maillon, s_map *m, SDL_Rect r, int pas)
{
    int i;
    int x = maillon->x, y = maillon->y;
    int haut = 1, bas = 1, gauche = 1, droite = 1;
    //HAUT avec pas
    for(i = 0;i < pas;i++)
    {
        if(caseVide(x, y - 1 - i, m, r) != VIDE
           || caseVide(x - 1, y - 1 - i, m, r) == PLEINE
           || caseVide(x + 1, y - 1 - i, m, r) == PLEINE)
        {
            haut = 0;
            break;
        }
    }
    if(caseVide(x, y - 1 - i, m, r) == PLEINE)
        haut = 0;
    if(haut == 1)
        maillon->possibilites[HAUT] = 1;//alors la case du haut est accessible
    else
        maillon->possibilites[HAUT] = 0;//sinon elle ne l'est pas

    //BAS avec pas
    for(i = 0;i < pas;i++)
    {
        if(caseVide(x, y + 1 + i, m, r) != VIDE
           || caseVide(x - 1, y + 1 + i, m, r) == PLEINE
           || caseVide(x + 1, y + 1 + i, m, r) == PLEINE)
        {
            bas = 0;
            break;
        }
    }
    if(caseVide(x, y + 1 + i, m, r) == PLEINE)
        bas = 0;
    if(bas == 1)
        maillon->possibilites[BAS] = 1;
    else
        maillon->possibilites[BAS] = 0;

    //GAUCHE avec pas
    for(i = 0;i < pas;i++)
    {
        if(caseVide(x - 1 - i, y, m, r) != VIDE
           || caseVide(x - 1 - i, y - 1, m, r) == PLEINE
           || caseVide(x - 1 - i, y + 1, m, r) == PLEINE)
        {
            gauche = 0;
            break;
        }
    }
    if(caseVide(x - 1 - i, y, m, r) == PLEINE)
        gauche = 0;
    if(gauche == 1)
        maillon->possibilites[GAUCHE] = 1;
    else
        maillon->possibilites[GAUCHE] = 0;

    //DROITE avec pas
    for(i = 0;i < pas;i++)
    {
        if(caseVide(x + 1 + i, y, m, r) != VIDE
           || caseVide(x + 1 + i, y - 1, m, r) == PLEINE
           || caseVide(x + 1 + i, y + 1, m, r) == PLEINE)
        {
            droite = 0;
            break;
        }
    }
    if(caseVide(x + 1 + i, y, m, r) == PLEINE)
        droite = 0;
    if(droite == 1)
        maillon->possibilites[DROITE] = 1;
    else
        maillon->possibilites[DROITE] = 0;
}

int caseVide(int x, int y, s_map *m, SDL_Rect r)
{
    if(x < r.x || x >= r.x + r.w
       || y < r.y || y >= r.y + r.h)
        return EN_DEHORS; // la case est en dehors de la zone
    else if(m->carte[x][y].pos.c.z == -1)
        return VIDE;
    else if(m->carte[x][y].pos.c.z >= 0)
        return PLEINE;
    else
    {
        fprintf(stderr, "\n Une case possède une valeur impossible");
        fprintf(stderr, "\n x %d y %d", x, y);
        exit(EXIT_FAILURE);
    }
}

int restePossibilites(s_maillon *maillon)
/// dit si il reste des chemins inexploités, s'il reste des possibilités parmis la chaine tout entière !
{
    int continuer = 1, i;
    s_maillon *maillonActuel = maillon;
    while(continuer)
    {
        for(i = 0;i < 4;i++)
            if(maillonActuel->possibilites[i] == 1)
                return 1;
        if(maillonActuel->precedant == NULL)
            continuer = 0;
        else
            maillonActuel = maillonActuel->precedant;
    }
    return 0;
}

int possibilitesMaillon(s_maillon *maillon)
/// retourne si il reste des possibilités au maillon
{
    int i;
    for(i = 0;i < 4;i++)
        if(maillon->possibilites[i] == 1)
            return 1;
    return 0;
}

void ajouterMaillonEnQueue(s_maillon *maillonDeFin, s_maillon *newMaillon)
/// ajoute un maillon à la fin de la chaine
{
    maillonDeFin->suivant = newMaillon;
    newMaillon->precedant = maillonDeFin;
    newMaillon->suivant = NULL;
}

s_maillon* defiler(s_maillon *file)
/// renvoie le premier élément de la file et l'enlève de la file
{
    if(file == NULL)
    {
        fprintf(stderr, "\nLa file est vide");
        exit(EXIT_FAILURE);
    }
    s_maillon *premier = malloc(sizeof(s_maillon));
    premier->x = file->x;
    premier->y = file->y;
    premier->suivant = file->suivant;
    premier->precedant = file->precedant;

    if(file->suivant != NULL)
    {
        file->x = file->suivant->x;
        file->y = file->suivant->y;
        file->suivant = file->suivant->suivant;
        file->precedant = NULL;
    }
    else
    {
        file = NULL;
    }

    return premier;
}

void enfiler(s_maillon *file, int x, int y)
/// enfile un élément à la suite de la file
{
    s_maillon *nv = malloc(sizeof(s_maillon));
    nv->x = x;
    nv->y = y;
    nv->suivant = NULL;
    if(file == NULL)
    {
        nv->precedant = NULL;
        file = nv;
    }
    else
    {
        s_maillon *actuel = file;
        while(actuel->suivant != NULL)
            actuel = actuel->suivant;
        nv->precedant = actuel;
        actuel->suivant = nv;
    }
}

int fileEstVide(s_maillon *file)
/// renvoie si la file est vide ou non
{
    if(file == NULL)
        return 1;
    else
        return 0;
}

void afficherFile(s_maillon *file)
/// affiche une file en entière
{
    s_maillon *actuel = file;
    if(file == NULL)
        fprintf(stderr, "\nLa file est vide");
    else
    {
        fprintf(stderr, "\n");
        while(actuel->suivant != NULL)
        {
            fprintf(stderr, " [%d, %d]", actuel->x, actuel->y);
            actuel = actuel->suivant;
        }
    }
}

























