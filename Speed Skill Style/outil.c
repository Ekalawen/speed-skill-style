
#include "include.h"


s_posC posVconvertPosC(s_posV posV)
{
    s_posC posC;
    posC.x = (int)(posV.x - 20) / 80;
    posC.y = (int)(posV.y + 20) / 80;
    posC.z = (int)posV.z;
    return posC;
}

s_posA posVconvertPosA(s_posV posV)
{
    s_posA posA;
    posA.x = 40 * ((posV.x - posV.y) / 80) + (LARGEUR_ECRAN / 2);
    posA.y = 20 * ((posV.x + posV.y) / 80) - (40 * (posV.z - 1));
    return posA;
}

s_posA posCconvertPosA(s_posC posC)
{
    s_posA posA;
    posA.x = 40 * (posC.x - posC.y) + (LARGEUR_ECRAN / 2);
    posA.y = 20 * (posC.x + posC.y) - (40 * (posC.z - 1));
    return posA;
}

void arrondirPosV(s_posV *posV, s_posC posC)
{
    posV->x = posC.x * 80;
    posV->y = posC.y * 80;
    posV->z = posC.z;
}

void blit(SDL_Surface *surf, SDL_Rect *R, SDL_Surface *ecran, s_posA *posA)
{
    SDL_Rect pos;
    pos.x = posA->x;
    pos.y = posA->y;
    if(R != NULL)
        SDL_BlitSurface(surf, R, ecran, &pos);
    else
        SDL_BlitSurface(surf, NULL, ecran, &pos);
}

int min(a, b)
{
    if(a <= b)
        return a;
    return b;
}

int max(a, b)
{
    if(a >= b)
        return a;
    return b;
}

int arround(double a)
{
    return a + 0.5;
}

int len(void *t)
///renvoie la taille du tableau t
{
    return sizeof(t);
}

int randint(int m, int M)
///renvoie un entier entre m et M inclue
{
    return floor(rand() % (M - m + 1) + m);
}

double rand01()
{
    return (double) rand() / RAND_MAX;
}

double randdouble(double m, double M)
///renvoie un flottant quelconque entre m et M inclue
{
    return rand01() * (M - m) + m;
}

int directionOpposee(int dir)
///renvoie la direction opposée
{
    switch(dir)
    {
    case HAUT:
        return BAS;
        break;
    case BAS:
        return HAUT;
        break;
    case GAUCHE:
        return DROITE;
        break;
    case DROITE:
        return GAUCHE;
        break;
    case HAUT_GAUCHE:
        return BAS_DROITE;
        break;
    case HAUT_DROITE:
        return BAS_GAUCHE;
        break;
    case BAS_GAUCHE:
        return HAUT_DROITE;
        break;
    case BAS_DROITE:
        return HAUT_GAUCHE;
        break;
    default:
        return -1;
        break;
    }
}

int directionRelative(s_posC c, s_posC nv)
///renvoie où est situé le nvPoint par rapport au centre
{
    //haut
    int haut, bas, gauche, droite; //distance cardinales entre les deux point
    //ces distances sont positives si le nv point est dans cette direction par rapport au centre
    haut = c.y - nv.y;
    bas = nv.y - c.y;
    gauche = c.x - nv.x;
    droite = nv.x - c.x;
    if(haut >= 0 && gauche >= 0)
        if(haut >= gauche)
            return HAUT;
        else
            return GAUCHE;
    else if(gauche >= 0 && bas >= 0)
        if(gauche >= bas)
            return GAUCHE;
        else
            return BAS;
    else if (bas >= 0 && droite >= 0)
        if(bas >= droite)
            return BAS;
        else
            return DROITE;
    else if(droite >= 0 && haut >= 0)
        if(droite >= haut)
            return DROITE;
        else
            return HAUT;
    else
    {
        fprintf(stderr, "\nErreur dans la fonction directionRelatives");
        fprintf(stderr, "\nc %d %d    nv %d %d", c.x, c.y, nv.x, nv.y);
    }
}

































