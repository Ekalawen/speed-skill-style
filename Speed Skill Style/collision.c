

#include "include.h"

int collisionJoueurEtage(s_joueur *joueur, s_posV *posV)
{
    //dit si le joueur est sur une case ou non
    //il peut-être jusqu'a sur 4 cases en même temps
    int x = joueur->pos.v.x;
    int y = joueur->pos.v.y;
    int w = joueur->pos.v.w;
    int h = joueur->pos.v.h;

    if(x >= posV->x + posV->w
       ||x + w <= posV->x
       ||y >= posV->y + posV->h
       ||y + h <= posV->y)
       return 0; //pas de collision
    return 1; //collision
}

int collisionJoueurAffichageEtage(s_joueur *joueur, s_posTotale *pos)
{
    //dit si l'affichage de l'image du joueur rentre en collision avec l'affichage de l'image d'une case
    int x = joueur->pos.a.x;
    int y = joueur->pos.a.y;
    int w = joueur->pos.v.w;
    int h = joueur->pos.v.h;
    if(x + w <= pos->a.x - 40
       ||x >= pos->a.x + pos->R.w + 40
       ||y + h <= pos->a.y
       ||y >= pos->a.y + pos->R.h)
       return 0; //pas de collision
    return 1; //collision
}

int caseVisible(s_posA *posA, int hauteur)
/// renvoie si l'affichage de la case est dans l'écran
{
    if(posA->x > 0 - 80
       &&posA->x < LARGEUR_ECRAN
       &&posA->y > 0 - (40 * hauteur) - 80
       &&posA->y < HAUTEUR_ECRAN)
       return 1; //on voit la case
    return 0; //on ne le voit pas
}

int collisionAffichageEtage(s_infoTile *caseBasse, s_infoTile *caseHaute)
{
    int xb = caseBasse->pos.c.x;
    int yb = caseBasse->pos.c.y;
    int zb = caseBasse->pos.c.z;
    int xh = caseHaute->pos.c.x;
    int yh = caseHaute->pos.c.y;
    int zh = caseHaute->pos.c.z;
    if(xh - xb == yh - yb
       &&yh - yb == zh - zb)
       return 1; //collision
    return 0; //pas de collision
}

int distancePlate(s_posC *case1, s_posC *case2)
{
    int dx, dy;
    dx = fabs(case1->x - case2->x);
    dy = fabs(case1->y - case2->y);
    return dx + dy;
}

int distancePlateCarre(s_posC *case1, s_posC *case2)
{
    int dx, dy;
    dx = fabs(case1->x - case2->x);
    dy = fabs(case1->y - case2->y);
    if(dx >= dy)
        return dx;
    else
        return dy;
}

int poteau(s_posC *case1, s_map *m)
{
    int i, j;
    for(i = case1->x - 1;i <= case1->x + 1;i++)
    {
        for(j = case1->y - 1;j <= case1->y + 1;j++)
        {
            if(i >= 0 && j >= 0 && i < m->x && j < m->y)
            {
                if(!(i == case1->x && j == case1->y))
                {
                    if(m->carte[i][j].pos.c.z > case1->z - TAILLE_POTEAU)
                    {
                        return 0; //ce n'est pas un poteau
                    }

                }

            }
        }
    }
    return 1; //c'est un poteau
}

s_posC* casesOccuppeesParJoueur(s_map *m, s_joueur *joueur)
///renvoie les cases sur lesquelles le joueur repose
{
    int i, j, k = 0;
    double xJ, yJ, wJ, hJ;
    int zJ;
    double xC, yC, wC, hC;
    int zC;
    s_posC *posOccupees = malloc(sizeof(s_posC) * 4);
    memset(posOccupees, -1, sizeof(s_posC) * 4);
    for(i = 0;i < m->x;i++)
    {
        for(j = 0;j < m->y;j++)
        {
            xJ = joueur->pos.v.x;
            yJ = joueur->pos.v.y;
            wJ = joueur->pos.v.w;
            hJ = joueur->pos.v.h;
            zJ = joueur->pos.v.z;
            xC = m->carte[i][j].pos.v.x;
            yC = m->carte[i][j].pos.v.y;
            wC = m->carte[i][j].pos.v.w;
            hC = m->carte[i][j].pos.v.h;
            zC = m->carte[i][j].pos.v.z;
            if(zJ != zC
               || xJ >= xC + wC
               || xJ + wJ <= xC
               || yJ >= yC + hC
               || yJ + hJ <= yC)
            ;
            else
            {
                posOccupees[k].x = i;
                posOccupees[k].y = j;
                posOccupees[k].z = zC;
                k ++;
            }
        }
    }
    return posOccupees;
}















































