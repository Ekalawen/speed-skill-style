


#include "include.h"


void updateEvents(s_input *in)
{
    SDL_Event event;
        in->button[SDL_BUTTON_WHEELUP] = 0;
        in->button[SDL_BUTTON_WHEELDOWN] = 0;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_KEYDOWN:
                in->key[event.key.keysym.sym] = 1;
                break;
            case SDL_KEYUP:
                in->key[event.key.keysym.sym] = 0;
                break;
            case SDL_MOUSEMOTION:
                in->mouseX = event.motion.x;
                in->mouseY = event.motion.y;
                in->mouseVecX = event.motion.xrel;
                in->mouseVecY = event.motion.yrel;
                break;
            case SDL_MOUSEBUTTONDOWN:
                in->button[event.button.button] = 1;
                break;
            case SDL_MOUSEBUTTONUP:
                    if(event.button.button != SDL_BUTTON_WHEELDOWN &&
                       event.button.button != SDL_BUTTON_WHEELUP)
                in->button[event.button.button] = 0;
                break;
            case SDL_QUIT:
                in->quit = 1;
                break;
            default:
                break;
        }
    }
}

void resetEvents(s_input *in)
{
    int i;
    for(i=0;i < SDLK_LAST;i++)
        in->key[i] = 0;
    in->mouseVecX = 0;
    in->mouseVecY = 0;
    for(i=0;i < 8;i++)
        in->button[i] = 0;
    in->quit = 0;
    SDL_GetMouseState(&in->mouseX, &in->mouseY);
}








































