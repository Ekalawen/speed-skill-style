

#ifndef LABYRINTHE_H
#define LABYRINTHE_H

    //propriétés d'une case
    enum {VIDE, PLEINE, EN_DEHORS};

    typedef struct s_maillon s_maillon;
    struct s_maillon
    {
        int x, y;
        int possibilites[4];
        s_maillon *suivant;
        s_maillon *precedant;
    };


    void genLabyrinthe(s_map *m, SDL_Rect r, int pas, int repetition, int xdebut, int ydebut);
    s_maillon* initMaillon(int x, int y, s_map *m, SDL_Rect r, int pas);
    int premierMaillon(s_maillon *maillon);
    int dernierMaillon(s_maillon *maillon);
    void majPossibiliteMaillon(s_maillon *maillon, s_map *m, SDL_Rect r, int pas);
    int caseVide(int x, int y, s_map *m, SDL_Rect r);
    int restePossibilites(s_maillon *maillon);
    int possibilitesMaillon(s_maillon *maillon);
    void ajouterMaillonEnQueue(s_maillon *maillonDeFin, s_maillon *newMaillon);
    s_maillon* defiler(s_maillon *file);
    void enfiler(s_maillon *file, int x, int y);
    int fileEstVide(s_maillon *file);
    void afficherFile(s_maillon *file);

#endif // LABYRINTHE_H





































